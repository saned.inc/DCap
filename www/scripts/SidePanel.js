﻿myApp.angular.factory('SidePanelService', ['$rootScope', 'CookieService', 'appServices', function ($rootScope, CookieService, appServices) {
    'use strict';

    function DrawMenu() {
        var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;
        var userRole = CookieService.getCookie('userRole') ? CookieService.getCookie('userRole') : 'seeker';
        var userLoggedIn = CookieService.getCookie('userLoggedIn') ? CookieService.getCookie('userLoggedIn') : null;
        $rootScope.IsVisitor = IsVisitor;

        var fw7 = myApp.fw7;

        if (IsVisitor == 'true') {
            $('#linkMenuToHome').css('display', 'block ');
            $('#linkMenuToNotifications').css('display', 'none');
            $('#linkMenuToApps').css('display', 'block');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToAbout').css('display', 'block');
            $('#linkMenuToProfile').css('display', 'none');
            $('#linkMenuToChangePassword').css('display', 'none');
            $('#linkMenuToLogin').css('display', 'block');
            $('#lblMenuLoginText').html('تسجيل دخول');
        }
        else {
            $('#linkMenuToHome').css('display', 'block');
            $('#linkMenuToNotifications').css('display', 'block');
            $('#linkMenuToApps').css('display', 'block');
            $('#linkMenuToContact').css('display', 'block');
            $('#linkMenuToAbout').css('display', 'block');
            $('#linkMenuToLogin').css('display', 'block');
            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                $('#linkMenuToProfile').css('display', 'block');
                if (userRole == 'seeker') {
                    $('#linkMenuToChangePassword').css('display', 'none');
                }
                else {
                    $('#linkMenuToChangePassword').css('display', 'block');
                }
                $('#lblMenuLoginText').html('تسجيل خروج');
            }
            else {
                $('#linkMenuToNotifications').css('display', 'none');
                $('#linkMenuToProfile').css('display', 'none');
                $('#linkMenuToChangePassword').css('display', 'none');
                $('#lblMenuLoginText').html('تسجيل دخول');
            }
        }

        setTimeout(function () {
            //$scope.$apply();
            // $rootScope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    return {
        DrawMenu: DrawMenu
    }

}]);