﻿/// <reference path="../js/angular.js" />
/// <reference path="../js/framework7.js" />
/// <reference path="../js/jquery-2.1.0.js" />

var isAndroid = Framework7.prototype.device.android === true;
var isIos = Framework7.prototype.device.ios === true;

Template7.global = {
    android: isAndroid,
    ios: isIos
};

var $$ = Dom7;

if (isAndroid) {
    $$('.view.navbar-through').removeClass('navbar-through').addClass('navbar-fixed');
    $$('.view .navbar').prependTo('.view .page');
}

if (isIos) {
    $$('.view.navbar-fixed').removeClass('navbar-fixed').addClass('navbar-through');
    $$('.view .navbar').prependTo('.view .page');
}

var myApp = {};

myApp.config = {
};

var compareTo = function () {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function (scope, element, attributes, ngModel) {

            ngModel.$validators.compareTo = function (modelValue) {
                return modelValue == scope.otherModelValue;
            };

            scope.$watch("otherModelValue", function () {
                ngModel.$validate();
            });
        }
    };
};

myApp.angular = angular.module('myApp', ['ErrorCatcher', 'jkAngularRatingStars', 'angAccordion']).config(function ($provide) {
    $provide.decorator("$exceptionHandler", function ($delegate, $injector, $log) {
        return function (exception, cause) {
            $log.error(exception)
            $delegate(exception, cause);
        };
    });
}).directive('homePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'homeController',
        scope: {},
        templateUrl: 'pages/home.html'
    };
}).directive('landingPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'landingController',
        scope: {},
        templateUrl: 'pages/landing.html'
    };
}).directive('loginPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'loginController',
        scope: {},
        templateUrl: 'pages/login.html'
    };
}).directive('forgetPassPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'forgetPasswordController',
        scope: {},
        templateUrl: 'pages/forgetPassword.html'
    };
}).directive('resetPasswordPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'resetPasswordController',
        scope: {},
        templateUrl: 'pages/resetPassword.html'
    };
}).directive('changePassPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'changePasswordController',
        scope: {},
        templateUrl: 'pages/changePassword.html'
    };
}).directive('activationPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'activationController',
        scope: {},
        templateUrl: 'pages/activation.html'
    };
}).directive('userProfilePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userProfileController',
        scope: {},
        templateUrl: 'pages/userProfile.html'
    };
}).directive('contactPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'contactController',
        scope: {},
        templateUrl: 'pages/contact.html'
    };
}).directive('editProfilePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'editProfileController',
        scope: {},
        templateUrl: 'pages/editProfile.html'
    };
}).directive('agentProfilePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'agentProfileController',
        scope: {},
        templateUrl: 'pages/agentProfile.html'
    };
}).directive('reportPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'reportController',
        scope: {},
        templateUrl: 'pages/report.html'
    };
}).directive('userNotificationPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userNotificationController',
        scope: {},
        templateUrl: 'pages/userNotification.html'
    };
}).directive('agentNotificationPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'agentNotificationController',
        scope: {},
        templateUrl: 'pages/agentNotification.html'
    };
}).directive('noResultPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'noResultController',
        scope: {},
        templateUrl: 'pages/noResult.html'
    };
}).directive('signupAgentWizardFirstPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'signupAgentWizardFirstController',
        scope: {},
        templateUrl: 'pages/signupAgentWizardFirst.html'
    };
}).directive('signupAgentWizardSecondPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'signupAgentWizardSecondController',
        scope: {},
        templateUrl: 'pages/signupAgentWizardSecond.html'
    };
}).directive('userTypePage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'userTypeController',
        scope: {},
        templateUrl: 'pages/userType.html'
    };
}).directive('agentsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'agentsController',
        scope: {},
        templateUrl: 'pages/agents.html'
    };
}).directive('agentDetailsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'agentDetailsController',
        scope: {},
        templateUrl: 'pages/agentDetails.html'
    };
}).directive('aboutPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'aboutController',
        scope: {},
        templateUrl: 'pages/about.html'
    };
}).directive('myRatingsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'myRatingsController',
        scope: {},
        templateUrl: 'pages/myRatings.html'
    };
}).directive('appsPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'appsController',
        scope: {},
        templateUrl: 'pages/apps.html'
    };
}).directive('searchPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'searchController',
        scope: {},
        templateUrl: 'pages/search.html'
    };
}).directive('homeIosPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'homeIosController',
        scope: {},
        templateUrl: 'pages/homeIos.html'
    };
}).directive('subCategoryPage', function () {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'subCategoryController',
        scope: {},
        templateUrl: 'pages/subCategory.html'
    };
});

myApp.angular.directive('fallbackSrc', function () {
    var fallbackSrc = {
        link: function postLink(scope, iElement, iAttrs) {
            iElement.bind('error', function () {
                angular.element(this).attr("src", iAttrs.fallbackSrc);
            });
        }
    }
    return fallbackSrc;
});

myApp.fw7 = {
    app: new Framework7({
        swipeBackPage: false,
        swipePanel: false,
        panelsCloseByOutside: true,
        animateNavBackIcon: true,
        material: isAndroid ? true : false,
        materialRipple: false,
        modalButtonOk: 'تم',
        modalButtonCancel: 'إلغاء'
    }),
    options: {
        dynamicNavbar: false,
        domCache: true
    },
    views: [],
    Countries: [],
    Cities: [],
    Categories: [],
    Advertisements: [],
    DelayBeforeScopeApply: 100,
    PageSize: 7
};