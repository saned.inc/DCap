﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('signupAgentWizardSecondController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var name;
    var userName;
    var password;
    var confirmPassword;

    function LoadCities(callBack) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        var cities = [];

        appServices.CallService('signupAgentWizardSecond', "GET", "api/v1/cities", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.cities = res;
            }

            callBack(cities);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadCategoriesAndSubs(callBack) {
        var categories = [];
        var subCategories = [];

        appServices.CallService('signupAgentWizardSecond', "GET", "api/v1/categories", '', function (res) {
            SpinnerPlugin.activityStop();
            if (res != null && res.length > 0) {

                angular.forEach(res, function (category, index) {
                    category.index = index;
                    $scope.maxNum = parseInt(category.category_number);

                    angular.forEach(category.children, function (subCategory, index) {
                        subCategory.index = index;
                        subCategories.push(subCategory);
                    });
                });
                $scope.categories = res;
                $scope.subCategories = subCategories;
            }

            callBack(categories);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function getImage() {
        navigator.camera.getPicture(uploadPhoto, function (message) {

        }, {
            quality: 100,
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
            allowEdit: false,
            encodingType: Camera.EncodingType.JPEG,
            //targetWidth: 300,
            //targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        });
    }

    function uploadPhoto(imageURI) {
        $scope.userImage = imageURI;

        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $document.ready(function () {
        app.onPageInit('signupAgentWizardSecond', function (page) {
            if ($rootScope.currentOpeningPage != 'signupAgentWizardSecond') return;
            $rootScope.currentOpeningPage = 'signupAgentWizardSecond';

        });

        app.onPageBeforeAnimation('signupAgentWizardSecond', function (page) {
            if ($rootScope.currentOpeningPage != 'signupAgentWizardSecond') return;
            $rootScope.currentOpeningPage = 'signupAgentWizardSecond';
            $scope.resetForm();

            name = page.query.name;
            userName = page.query.username;
            password = page.query.password;
            confirmPassword = page.query.confirmpassword;

            $scope.disableAll = false;

            LoadCities(function (cities) {
                LoadCategoriesAndSubs(function (result) {

                });
            });
        });

        app.onPageReinit('signupAgentWizardSecond', function (page) {

            $scope.resetForm();
        });

        app.onPageAfterAnimation('signupAgentWizardSecond', function (page) {
            if ($rootScope.currentOpeningPage != 'signupAgentWizardSecond') return;
            $rootScope.currentOpeningPage = 'signupAgentWizardSecond';

            name = page.query.name;
            userName = page.query.username;
            password = page.query.password;
            confirmPassword = page.query.confirmpassword;

            $scope.resetForm();

        });

        $scope.resetForm = function () {
            $scope.AgentSecondWizardReset = false;
            $scope.agentSecondWizardForm.city = null;
            $scope.agentSecondWizardForm.category = null;
            $scope.agentSecondWizardForm.subCategory = null;
            $scope.agentSecondWizardForm.userImage = null;

            $('#linkSignUpAgentCity .item-after').html('المدينة');
            $('#linkSignUpAgentCategory .item-after').html('التصنيفات الرئيسية');
            $('#linkSignUpAgentSubCategory .item-after').html('التصنيفات الفرعية');

            if (typeof $scope.AgentSecondWizardForm != 'undefined' && $scope.AgentSecondWizardForm != null) {
                $scope.AgentSecondWizardForm.$setPristine(true);
                $scope.AgentSecondWizardForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.AddImage = function () {
            getImage();
        };

        $scope.FilterSubCategories = function (categories) {
            var subCategories =[];

            angular.forEach(categories, function (category, index) {
                angular.forEach(category.children, function (subCategory, index) {
                    subCategories.push(subCategory);
                });
            });

            $scope.subCategories = subCategories;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.submitForm = function (isValid) {
            $scope.AgentSecondWizardReset = true;
            $scope.isImageEmpty = typeof $scope.userImage == "undefined" || $scope.userImage == null || $scope.userImage == '' || $scope.userImage == ' ' ? true : false;

            if (isValid) { //&& $scope.isImageEmpty == false
                var subCategories = [];
                angular.forEach($scope.agentSecondWizardForm.subCategory, function (subCategory, index) {
                    subCategories.push(subCategory.id);
                });

                subCategories = JSON.stringify(subCategories);

                var user = {
                    'city_id': $scope.agentSecondWizardForm.city.id,
                    'categories': subCategories,
                    'national_image': $scope.userImage,
                    'name': name,
                    'username': userName,
                    'password': password,
                    'password_confirmation': confirmPassword,
                    'email': 'fff@fff.com'
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('signupAgentWizardSecond', "POST", "api/v1/register", user, function (res2) {
                    SpinnerPlugin.activityStop();
                    if (res2 != null) {
                        CookieService.setCookie('UserID', res2.id);
                        CookieService.setCookie('appToken', res2.api_token);
                        CookieService.setCookie('USName', res2.username);
                        CookieService.setCookie('Visitor', false);
                        CookieService.setCookie('loginUsingSocial', false);
                        CookieService.setCookie('userLoggedIn', JSON.stringify(res2));
                        CookieService.setCookie('UserEntersCode', true);

                        language.openFrameworkModal('نجاح', 'تم تسجيل بياناتك بنجاح .', 'alert', function () {
                            if (isAndroid) {
                                helpers.GoToPage('homeIos', null);
                            }
                            else {
                                helpers.GoToPage('homeIos', null);
                            }
                        });
                    }
                });
            }
        };

        $scope.CountSelectedSubs = function (SubCategories) {
            $scope.maxCategoriesExceeded = $scope.maxNum < SubCategories.length ? true : false;

            if ($scope.maxNum <= SubCategories.length) {
                $$('div[data-select-name="subCategory"] ul li').each(function (element, index) {
                    $(this).addClass('disabled');
                });
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.form = {};
        $scope.agentSecondWizardForm = {};

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;



        app.init();
    });

}]);

