﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('signupAgentController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $document.ready(function () {
        app.onPageInit('signupAgent', function (page) {
            if ($rootScope.currentOpeningPage != 'signupAgent') return;
            $rootScope.currentOpeningPage = 'signupAgent';
        });

        app.onPageBeforeAnimation('signupAgent', function (page) {
            if ($rootScope.currentOpeningPage != 'signupAgent') return;
            $rootScope.currentOpeningPage = 'signupAgent';

            $scope.resetForm();
        });

        app.onPageReinit('signupAgent', function (page) {
           
        });

        app.onPageAfterAnimation('signupAgent', function (page) {
            if ($rootScope.currentOpeningPage != 'signupAgent') return;
            $rootScope.currentOpeningPage = 'signupAgent';

            $scope.resetForm();

        });



        $scope.resetForm = function () {
            $scope.signUpAgentReset = false;
            $scope.agentForm.mobile = null;

            if (typeof $scope.SignupAgentForm != 'undefined' && $scope.SignupAgentForm != null) {
                $scope.SignupAgentForm.$setPristine(true);
                $scope.SignupAgentForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }



        $scope.submitForm = function (isValid) {
            $scope.signUpAgentReset = true;
            if (isValid) {
                $('#agentSignUpButton').prop('disabled', 'disabled');
                var agent = {
                    'phone': $scope.agentForm.mobile,
                    'user_type': CookieService.getCookie('userRole')
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('signupAgent', "POST", "api/v1/register/phone", agent, function (res2) {
                    $('#agentSignUpButton').prop('disabled', '');
                    if (res2 != null) {
                        CookieService.setCookie('UserID', res2);
                        CookieService.setCookie('UserEntersCode', false);
                        SpinnerPlugin.activityStop();
                        $scope.resetForm();
                        language.openFrameworkModal('نجاح', 'تم تسجيل بياناتك بنجاح , سوف يتم إرسال كود تفعيل علي رقم الجوال .', 'alert', function () {
                            helpers.GoToPage('activation', null);
                        });
                    }
                    else {
                        SpinnerPlugin.activityStop();
                    }
                });
            }
        };

        $scope.form = {};
        $scope.agentForm = {};

        $scope.GoBack = function () {
            $scope.resetForm();
            helpers.GoBack();
        };

        $scope.GoToLogin = function () {
            helpers.GoToPage('login', null);
        };

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();

    });
}]);

