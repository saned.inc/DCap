﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('signupUserController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $document.ready(function () {
        app.onPageInit('signupUser', function (page) {
            if ($rootScope.currentOpeningPage != 'signupUser') return;
            $rootScope.currentOpeningPage = 'signupUser';
        });

        app.onPageReinit('signupUser', function (page) {
            
        });

        app.onPageBeforeAnimation('signupUser', function (page) {
            if ($rootScope.currentOpeningPage != 'signupUser') return;
            $rootScope.currentOpeningPage = 'signupUser';
            $scope.resetForm();
        });

        app.onPageAfterAnimation('signupUser', function (page) {
            if ($rootScope.currentOpeningPage != 'signupUser') return;
            $rootScope.currentOpeningPage = 'signupUser';
            
        });

        

        $scope.resetForm = function () {
            $scope.signUpUserReset = false;
            $scope.userForm.mobile = null;
            $scope.user = {};
            if (typeof $scope.SignupUserForm != 'undefined' && $scope.SignupUserForm != null) {
                $scope.SignupUserForm.$setPristine(true);
                $scope.SignupUserForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        

        $scope.submitForm = function (isValid) {
            $scope.signUpUserReset = true;
            if (isValid) {
                $('#userSignUp').prop('disabled', 'disabled');
                var user = {
                    'phone': $scope.userForm.mobile,
                    'user_type': CookieService.getCookie('userRole')
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('signupUser', "POST", "api/v1/register/phone", user, function (res2) {
                    $('#userSignUp').prop('disabled', '');
                    if (res2 != null) {
                        CookieService.setCookie('UserID', res2);
                        CookieService.setCookie('UserEntersCode', false);
                        SpinnerPlugin.activityStop();
                        language.openFrameworkModal('نجاح', 'تم تسجيل بياناتك بنجاح , سوف يتم إرسال كود تفعيل علي رقم الجوال .', 'alert', function () {
                            helpers.GoToPage('activation', null);
                        });
                    }
                    else {
                        SpinnerPlugin.activityStop();
                    }
                });
            }
        };

        $scope.form = {};
        $scope.userForm = {};

        $scope.GoBack = function () {
            $scope.resetForm();
            helpers.GoBack();
        };

        $scope.GoToLogin = function () {
            helpers.GoToPage('login', null);
        };

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();

    });
}]);

