﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('entryController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $document.ready(function () {
        app.onPageInit('entry', function (page) {
            if ($rootScope.currentOpeningPage != 'entry') return;
            $rootScope.currentOpeningPage = 'entry';

        });

        app.onPageBeforeAnimation('entry', function (page) {
            if ($rootScope.currentOpeningPage != 'entry') return;
            $rootScope.currentOpeningPage = 'entry';

        });

        app.onPageReinit('entry', function (page) {
            if ($rootScope.currentOpeningPage != 'entry') return;
            $rootScope.currentOpeningPage = 'entry';

        });

        app.onPageAfterAnimation('entry', function (page) {
            if ($rootScope.currentOpeningPage != 'entry') return;
            $rootScope.currentOpeningPage = 'entry';

        });

        app.init();
    });

    $scope.GoToLogin = function () {
        CookieService.setCookie('Visitor', false);
        helpers.GoToPage('login', null);
    };

    $scope.GoBack = function () {
        helpers.GoBack();
    };

    $scope.GoToSignup = function () {
        var userRole = CookieService.getCookie('userRole');
        CookieService.setCookie('Visitor', false);

        if (userRole == 'seeker') {
            helpers.GoToPage('signupUser', null);
        }
        else {
            helpers.GoToPage('signupAgent', null);
        }
    };

    $scope.Pass = function () {
        var isAndroid = Framework7.prototype.device.android === true;
        CookieService.setCookie('Visitor', true);
        CookieService.removeCookie('userLoggedIn')

        if (isAndroid) {
            helpers.GoToPage('homeIos', null);
        }
        else {
            helpers.GoToPage('homeIos', null);
        }
    };

}]);

