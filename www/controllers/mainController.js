﻿/// <reference path="../js/jquery-2.1.0.js" />
/// <reference path="../js/framework7.min.js" />
/// <reference path="../js/angular.js" />

myApp.angular.controller('mainController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var mainView = fw7.views['.view-main'];
    var app = myApp.fw7.app;
    $rootScope.currentOpeningPage = 'userType';


    $scope.GoToHome = function () {
        helpers.GoToPage('homeIos', null);
    };

    $scope.GoToProfile = function () {
        var userRole = CookieService.getCookie('userRole');
        if (userRole == 'seeker') {
            helpers.GoToPage('userProfile', null);
        }
        else {
            helpers.GoToPage('agentProfile', null);
        }
    };

    $scope.GoToNotifications = function () {
        var userRole = CookieService.getCookie('userRole');
        if (userRole == 'seeker') {
            helpers.GoToPage('userNotification', null);
        }
        else {
            helpers.GoToPage('agentNotification', null);
        }
    };

    $scope.GoToApps = function () {
        helpers.GoToPage('apps', null);
    };

    $scope.GoToAboutApplication = function () {
        helpers.GoToPage('about', null);
    };

    $scope.GoToContact = function () {
        helpers.GoToPage('contact', null);
    };

    $scope.GoToChangePassword = function () {
        helpers.GoToPage('changePass', null);
    };

    $scope.GoToLogin = function () {
        CookieService.removeCookie('appToken');
        CookieService.removeCookie('USName');
        CookieService.removeCookie('userLoggedIn');
        CookieService.removeCookie('userRole');
        CookieService.setCookie('Visitor', false);
        helpers.GoToPage('userType', null);
    };

    SidePanelService.DrawMenu();

    $document.ready(function () {
        SpinnerPlugin.activityStop();
    });

}]);


