﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('agentProfileController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var isOwner = false;

    function GroupSubCategoriesForEachParent(SubCategories) {
        var allAgentCategories = [];
        var categoryId = [];
        var subCategoryId = [];

        angular.forEach(SubCategories, function (subCategory, index) {
            allAgentCategories.push(subCategory.parent);
        });

        allAgentCategories = allAgentCategories.filter(el => {
            if (categoryId.indexOf(el.id) === -1) {
                categoryId.push(el.id);
                return true;
            }
        });

        angular.forEach(allAgentCategories, function (category, index) {
            category.subCategories = SubCategories.filter(el => {
                if (el.parent.id === category.id) {
                    return true;
                }
            });
        });

        angular.forEach(allAgentCategories, function (category, index) {
            angular.forEach(category.subCategories, function (subCategory, index) {
                subCategory.parent = null;
            });
        });

        return allAgentCategories;
    }

    function LoadAgentData(user) {
        $scope.username = typeof user != 'undefined' && user != null && typeof user.username != 'undefined' && user.username != 'null' ? user.username : '';
        $scope.name = typeof user != 'undefined' && user != null && typeof user.name != 'undefined' && user.name != 'null' ? user.name : '';
        $scope.mobile = typeof user != 'undefined' && user != null && typeof user.phone != 'undefined' && user.phone != 'null' ? user.phone : '';
        $scope.cityName = typeof user != 'undefined' && user != null && user.city != null && typeof user.city.name != 'undefined' && user.city.name != 'null' ? user.city.name : '';
        $scope.whatsapp = typeof user != 'undefined' && user != null && typeof user.whatsapp != 'undefined' && user.whatsapp != 'null' ? user.whatsapp : '';
        $scope.facebook = typeof user != 'undefined' && user != null && typeof user.facebook != 'undefined' && user.facebook != 'null' ? user.facebook : '';
        $scope.google = typeof user != 'undefined' && user != null && typeof user.google != 'undefined' && user.google != 'null' ? user.google : '';
        $scope.twitter = typeof user != 'undefined' && user != null && typeof user.twitter != 'undefined' && user.twitter != 'null' ? user.twitter : '';
        $scope.instagram = typeof user != 'undefined' && user != null && typeof user.instagram != 'undefined' && user.instagram != 'null' ? user.instagram : '';
        $scope.description = typeof user != 'undefined' && user != null && typeof user.description != 'undefined' && user.description != 'null' ? user.description : '';

        $scope.IsUserNameExists = typeof user != 'undefined' && user.username != null && user.username != '' && user.username != ' ' ? true : false;
        $scope.IsNameExists = typeof user != 'undefined' && user.name != null && user.name != '' && user.name != ' ' ? true : false;
        $scope.IsMobileExists = typeof user != 'undefined' && user.phone != null && user.phone != '' && user.phone != ' ' ? true : false;
        $scope.IsCityExists = typeof user != 'undefined' && user.city != null && user.city.name != '' && user.city.name != ' ' ? true : false;
        $scope.IsWhatsAppExists = typeof user != 'undefined' && user.whatsapp != null && user.whatsapp != '' && user.whatsapp != ' ' ? true : false;
        $scope.IsFacebookExists = typeof user != 'undefined' && user.facebook != null && user.facebook != '' && user.facebook != ' ' ? true : false;
        $scope.IsGooglePlusExists = typeof user != 'undefined' && user.google != null && user.google != '' && user.google != ' ' ? true : false;
        $scope.IsInstagramExists = typeof user != 'undefined' && user.instagram != null && user.instagram != '' && user.instagram != ' ' ? true : false;
        $scope.IsTwitterExists = typeof user != 'undefined' && user.twitter != null && user.twitter != '' && user.twitter != ' ' ? true : false;
        $scope.IsDescriptionExists = typeof user != 'undefined' && user.description != null && user.description != '' && user.description != ' ' ? true : false;
        $scope.IsSocialExists = $scope.IsWhatsAppExists || $scope.IsFacebookExists || $scope.IsGooglePlusExists || $scope.IsInstagramExists || $scope.IsTwitterExists ? true : false;
        $scope.IsAgentCategoriesExists = typeof $scope.categories != 'undefined' && $scope.categories != null && $scope.categories.length > 0 ? true : false;

        if (typeof user.image != 'undefined' && user.image != null && user.image != '' && user.image != ' ') {
            $scope.photoUrl = user.image;
        }
        else {
            $scope.photoUrl = 'http://placehold.it/100?text=image';
        }

        if (typeof user.national_image != 'undefined' && user.national_image != null && user.national_image != '' && user.national_image != ' ') {
            $scope.national_image = user.national_image;
        }
        else {
            $scope.national_image = 'http://placehold.it/250x100?text=image';
        }

        

        //$scope.categories = GroupSubCategoriesForEachParent(user.categories);
    }

    $document.ready(function () {
        app.onPageInit('agentProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'agentProfile') return;
            $rootScope.currentOpeningPage = 'agentProfile';

        });

        app.onPageBeforeAnimation('agentProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'agentProfile') return;
            $rootScope.currentOpeningPage = 'agentProfile';

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('agentProfile', 'POST', "api/v1/profile", '', function (userData) {
                SpinnerPlugin.activityStop();
                if (userData) {
                    $scope.categories = GroupSubCategoriesForEachParent(userData.categories);
                    userData.categories = $scope.categories;
                    CookieService.setCookie('userLoggedIn', JSON.stringify(userData));
                    LoadAgentData(userData);
                }

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });

            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;


            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageAfterAnimation('agentProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'agentProfile') return;
            $rootScope.currentOpeningPage = 'agentProfile';

        });

        app.onPageReinit('agentProfile', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.openEditProfile = function (userType) {
            helpers.GoToPage('editProfile', { userType: userType });
        };

        $scope.GoToMyRatings = function () {
            helpers.GoToPage('myRatings', null);
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        $scope.EditMobile = function () {
            app.prompt('من فضلك أدخل رقم الجوال الجديد', 'رقم الجوال الجديد', function (value) {

                if (helpers.isNumber(value) && value.match(/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/)) {
                    SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

                    var params = {
                        'phone': value
                    };

                    appServices.CallService('agentProfile', 'POST', "api/v1/phone/edit", params, function (settings) {
                        SpinnerPlugin.activityStop();
                        if (settings != null) {
                            CookieService.setCookie('NewMobileToEdit', value);
                            helpers.GoToPage('activation', null);
                        }
                    });
                }
                else {
                    language.openFrameworkModal('تنبيه', 'من فضلك أدخل رقم جوال صحيح', 'alert', function () { });
                }
            });
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        app.init();
    });

}]);

