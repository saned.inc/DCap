﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('activationController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $document.ready(function () {
        app.onPageInit('activation', function (page) {
            if ($rootScope.currentOpeningPage != 'activation') return;
            $rootScope.currentOpeningPage = 'activation';

        });

        app.onPageBeforeAnimation('activation', function (page) {
            if ($rootScope.currentOpeningPage != 'activation') return;
            $rootScope.currentOpeningPage = 'activation';
            fromPage = page.fromPage.name;

            $scope.resetForm();
        });

        app.onPageReinit('activation', function (page) {
            if ($rootScope.currentOpeningPage != 'activation') return;
            $rootScope.currentOpeningPage = 'activation';

            $scope.resetForm();
        });

        app.onPageAfterAnimation('activation', function (page) {
            if ($rootScope.currentOpeningPage != 'activation') return;
            $rootScope.currentOpeningPage = 'activation';
         
            fromPage = page.fromPage.name;
            $scope.resendCode = fromPage == 'userProfile' || fromPage == 'agentProfile' ? false : true;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
            $scope.resetForm();

        });

        $scope.resetForm = function () {
            $scope.activationForm.code = null;
            if (typeof $scope.ActivationForm != 'undefined' && $scope.ActivationForm != null) {
                $scope.ActivationForm.$setPristine(true);
                $scope.ActivationForm.$setUntouched();
            }
        }

        $scope.form = {};
        $scope.activationForm = {};

        $scope.submitForm = function (isValid) {

            if (isValid) {
                var code = $scope.activationForm.code;
                var mobileNumberToVerify=CookieService.getCookie('NewMobileToEdit');
                var params = {};
                var methodName = 'api/v1/register/activation';
                var userRole = CookieService.getCookie('userRole');
                var isAndroid = Framework7.prototype.device.android === true;

                if (fromPage == 'userProfile' || fromPage == 'agentProfile') {

                    methodName = 'api/v1/phone/update';

                    params = {
                        'phone': mobileNumberToVerify,
                        'activation_code': code
                    };
                }
                else {

                    methodName = 'api/v1/register/activation';

                    params = {
                        'activation_code': code
                    };
                }

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('activation', "POST", methodName, params, function (res) {
                    SpinnerPlugin.activityStop();
                    if (res != null) {

                        if (fromPage != 'userProfile' && fromPage != 'agentProfile') {
                            if (methodName = 'api/v1/register/activation') {
                                CookieService.setCookie('appToken', res.api_token);
                                CookieService.setCookie('USName', res.username);
                                CookieService.setCookie('Visitor', false);
                                CookieService.setCookie('loginUsingSocial', false);
                                CookieService.setCookie('userLoggedIn', JSON.stringify(res));
                            }
                            CookieService.setCookie('confirmationCode', res.activation_token);
                            CookieService.setCookie('appToken', res.api_token);
                        }

                        language.openFrameworkModal('نجاح', 'تم التأكد من رقم الجوال بنجاح .', 'alert', function () {
                            if (fromPage == 'userProfile') {
                                helpers.GoToPage('userProfile', null);
                            }
                            else if (fromPage == 'agentProfile') {
                                helpers.GoToPage('agentProfile', null);
                            }
                            else if (fromPage == 'forgetPass') {
                                helpers.GoToPage('resetPassword', null);
                            }
                            else if (fromPage == 'login' && userRole == 'seeker') {
                                if (isAndroid) {
                                    helpers.GoToPage('homeIos', null);
                                }
                                else {
                                    helpers.GoToPage('homeIos', null);
                                }
                            }
                            else if (fromPage == 'login' && userRole == 'agent') {
                                helpers.GoToPage('signupAgentWizardFirst', null);
                            }
                        });
                    }
                });
            }
        };

        $scope.ResendConfirmationCode = function () {
            var email = CookieService.getCookie('confirmationMail');
            var params = {
                'email': email
            };

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('activation', "POST", "api/v1/password/resend", params, function (res) {
                SpinnerPlugin.activityStop();
                if (res != null) {
                    language.openFrameworkModal('نجاح', 'تم إعادة إرسال الكود بنجاح .', 'alert', function () { });
                }
            });
        };

        app.init();
    });

}]);

