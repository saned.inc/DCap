﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('signupAgentWizardFirstController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    $document.ready(function () {
        app.onPageInit('signupAgentWizardFirst', function (page) {
            if ($rootScope.currentOpeningPage != 'signupAgentWizardFirst') return;
            $rootScope.currentOpeningPage = 'signupAgentWizardFirst';

        });

        app.onPageBeforeAnimation('signupAgentWizardFirst', function (page) {
            if ($rootScope.currentOpeningPage != 'signupAgentWizardFirst') return;
            $rootScope.currentOpeningPage = 'signupAgentWizardFirst';
            $scope.resetForm();
        });

        app.onPageReinit('signupAgentWizardFirst', function (page) {

            $scope.resetForm();
        });

        app.onPageAfterAnimation('signupAgentWizardFirst', function (page) {
            if ($rootScope.currentOpeningPage != 'signupAgentWizardFirst') return;
            $rootScope.currentOpeningPage = 'signupAgentWizardFirst';

            $scope.resetForm();
        });

        $scope.resetForm = function () {
            $scope.agentFirstWizardReset = false;
            $scope.agentFirstWizardForm.name = null;
            $scope.agentFirstWizardForm.username = null;
            $scope.agentFirstWizardForm.password = null;
            $scope.agentFirstWizardForm.confirmpassword = null;
            if (typeof $scope.SignupAgentFirstWizardForm != 'undefined' && $scope.SignupAgentFirstWizardForm != null) {
                $scope.SignupAgentFirstWizardForm.$setPristine(true);
                $scope.SignupAgentFirstWizardForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.GoBack = function () {
            helpers.GoBack();
        };

        $scope.submitForm = function (isValid) {
            $scope.agentFirstWizardReset = true;
            if (isValid) {
                var user = {
                    'name': $scope.agentFirstWizardForm.name,
                    'username': $scope.agentFirstWizardForm.username,
                    'password': $scope.agentFirstWizardForm.password,
                    'confirmpassword': $scope.agentFirstWizardForm.confirmpassword
                };

                helpers.GoToPage('signupAgentWizardSecond', {
                    name: $scope.agentFirstWizardForm.name,
                    username: $scope.agentFirstWizardForm.username,
                    password: $scope.agentFirstWizardForm.password,
                    confirmpassword: $scope.agentFirstWizardForm.confirmpassword
                });
            }
        };

        $scope.Pass = function () {
            var loginEmail = CookieService.getCookie('agentMobile');
            var loginPass = CookieService.getCookie('confirmationCode');

            var isAndroid = Framework7.prototype.device.android === true;
            var userRole = CookieService.getCookie('userRole');

            var params = {
                'phone': loginEmail,
                'password': loginPass,
                'user_type': userRole
            };

            appServices.CallService('login', "POST", "api/v1/check/phone", params, function (resToken) {
                SpinnerPlugin.activityStop();
                if (resToken != null && resToken != 'not found') {
                    CookieService.setCookie('appToken', resToken.api_token);
                    CookieService.setCookie('USName', resToken.username);
                    CookieService.setCookie('Visitor', false);
                    CookieService.setCookie('loginUsingSocial', false);
                    CookieService.setCookie('userLoggedIn', JSON.stringify(resToken));
                    SidePanelService.DrawMenu();
                    if (typeof resToken.image != 'undefined' && resToken.image != null && resToken.image != '' && resToken.image != ' ') {
                        CookieService.setCookie('usrPhoto', resToken.image);
                    }
                    else {
                        CookieService.removeCookie('usrPhoto');
                    }

                    if (isAndroid) {
                        helpers.GoToPage('homeIos', null);
                    }
                    else {
                        helpers.GoToPage('homeIos', null);
                    }
                }
                else {
                    if (userRole == 'seeker') {
                        helpers.GoToPage('signupUser', null);
                    }
                    else {
                        helpers.GoToPage('signupAgent', null);
                    }
                }
            });
        };

        $scope.form = {};
        $scope.agentFirstWizardForm = {};

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;


        app.init();
    });

}]);

