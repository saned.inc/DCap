﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('loginController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var pageName = '';
    var mobileEntered = false;

    function RegisterDevice(callBack) {
        //helpers.RegisterDevice(function (result) {
        //    callBack(true);
        //});
        callBack(true);
    }

    function CheckMobileExist(Mobile, Password, callBack) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

        var isAndroid = Framework7.prototype.device.android === true;
        var userRole = CookieService.getCookie('userRole');

        var params = {
            'phone': Mobile,
            'password': Password,
            'user_type': userRole
        };

        appServices.CallService('login', "POST", "api/v1/check/phone", params, function (resToken) {
            SpinnerPlugin.activityStop();
            if (resToken != null && resToken.message != 'notfound' && resToken != 'passwordWrong') {
                if (userRole == 'seeker') {
                    $scope.hideAgentPassword = true;
                }
                else {
                    $scope.hideAgentPassword = false;
                }
                callBack(true);
            }
            else {
                if (resToken != 'passwordWrong') {
                    var userOrAgent = {
                        'phone': Mobile,
                        'user_type': userRole
                    };

                    SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                    appServices.CallService('login', "POST", "api/v1/register/phone", userOrAgent, function (res2) {
                        SpinnerPlugin.activityStop();
                        if (res2 != null) {
                            CookieService.setCookie('UserID', res2);
                            CookieService.setCookie('UserEntersCode', false);
                            
                            language.openFrameworkModal('نجاح', 'تم تسجيل بياناتك بنجاح , سوف يتم إرسال كود تفعيل علي جوالك .', 'alert', function () {
                                helpers.GoToPage('activation', null);
                            });
                        }
                        callBack(false);
                    });
                    $scope.hideAgentPassword = true;
                }
                else {
                    if (userRole == 'seeker') {
                        $scope.hideAgentPassword = true;
                        callBack(true);
                    }
                    else {
                        $scope.hideAgentPassword = false
                        callBack(true);
                    }
                }
            }
        });
    }

    function CheckCredentials(Mobile, Password, callBack) {
        RegisterDevice(function (result) {

            var isAndroid = Framework7.prototype.device.android === true;
            var userRole = CookieService.getCookie('userRole');

            var params = {
                'phone': Mobile,
                'password': Password,
                'user_type': userRole
            };

            appServices.CallService('login', "POST", "api/v1/check/phone", params, function (resToken) {
                SpinnerPlugin.activityStop();
                if (resToken != null && resToken.message != 'notfound' && resToken != 'passwordWrong') {
                    CookieService.setCookie('appToken', resToken.api_token);
                    CookieService.setCookie('USName', resToken.username);
                    CookieService.setCookie('Visitor', false);
                    CookieService.setCookie('loginUsingSocial', false);
                    CookieService.setCookie('userLoggedIn', JSON.stringify(resToken));
                    SidePanelService.DrawMenu();
                    if (typeof resToken.image != 'undefined' && resToken.image != null && resToken.image != '' && resToken.image != ' ') {
                        CookieService.setCookie('usrPhoto', resToken.image);
                    }
                    else {
                        CookieService.removeCookie('usrPhoto');
                    }

                    if (isAndroid) {
                        helpers.GoToPage('homeIos', null);
                    }
                    else {
                        helpers.GoToPage('homeIos', null);
                    }
                }
                else {
                    if (resToken == 'passwordWrong') {
                        if (userRole == 'seeker') {
                            language.openFrameworkModal('خطأ', 'رقم الجوال خاطيء أو تم تسجيله من قبل لمستخدم أخر', 'alert', function () { });
                            callBack(false);
                        }
                        else {
                            language.openFrameworkModal('خطأ', 'رقم الجوال أو كلمة السر خاطئين', 'alert', function () { });
                            callBack(false);
                        }
                    }
                    else {
                        callBack(false);
                    }
                }
            });
            callBack(false);
        });

        callBack(true);
    }

    $document.ready(function () {
        app.onPageInit('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';

            mobileEntered = false;
        });

        app.onPageBeforeAnimation('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';
            SpinnerPlugin.activityStop();
            $scope.resetForm();
        });

        app.onPageAfterAnimation('login', function (page) {
            if ($rootScope.currentOpeningPage != 'login') return;
            $rootScope.currentOpeningPage = 'login';
            $scope.resetForm();

            pageName = page.fromPage.name;
        });

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

    $scope.resetForm = function () {
        $scope.loginReset = false;
        $scope.loginForm.mobile = null;
        $scope.loginForm.password = null;
        mobileEntered = false;


        var userRole = CookieService.getCookie('userRole');
        $scope.hidePassword = userRole == 'seeker' ? true : false;
        $scope.hideAgentPassword = true;

        if (typeof $scope.LoginForm != 'undefined' && $scope.LoginForm != null) {
            $scope.LoginForm.$setPristine(true);
            $scope.LoginForm.$setUntouched();
        }

        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $scope.GoBack = function () {
        if (pageName == 'activation' || pageName == 'signupAgentWizardSecond') {
            helpers.GoToPage('userType', null);
        }
        else {
            helpers.GoBack();
        }
    };

    $scope.GoToForgetPassword = function () {
        helpers.GoToPage('forgetPass', null);
    };

    $scope.form = {};
    $scope.loginForm = {};   
    $scope.submitForm = function (isValid) {
        $scope.loginReset = true;
        var userRole = CookieService.getCookie('userRole');

        if (isValid) {
            var loginEmail = $scope.loginForm.mobile, loginPass = $scope.loginForm.password;

            CookieService.setCookie('agentMobile', $scope.loginForm.mobile);

            if (mobileEntered == false) {
                CheckMobileExist(loginEmail, loginPass, function (result) {
                    mobileEntered = true;
                    if (userRole == 'seeker' && result == true) {
                        CheckCredentials(loginEmail, loginPass, function (result) {

                        });
                    }
                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            }
            else {
                CheckCredentials(loginEmail, loginPass, function (result) {
                    
                });
            }
        }
    };
}]);

