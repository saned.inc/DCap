﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('appsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var isAppsPull = true;
    var loadingApps = false;
    var allApps = [];

    function LoadApps(callBack) {
        if (!isAppsPull) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        }

        allApps = [];
        app.attachInfiniteScroll('#divInfiniteApps');

        var params = {
            'page_size': fw7.PageSize,
            'page': 0
        };

        appServices.CallService('apps', "POST", "api/v1/applications", params, function (res) {
            if (!isAppsPull) {
                SpinnerPlugin.activityStop();
            }
            if (res.data != null && res.data.length > 0) {
                $$('.page[data-page="apps"]').removeClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'relative');

                angular.forEach(res.data, function (app) {
                    allApps.push(app);
                });

                $scope.apps = allApps;
            }
            else {
                $$('.page[data-page="apps"]').addClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'absolute');
            }

            callBack(allApps);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $document.ready(function () {
        app.onPageInit('apps', function (page) {
            if ($rootScope.currentOpeningPage != 'apps') return;
            $rootScope.currentOpeningPage = 'apps';

            $$('#divInfiniteApps').on('ptr:refresh', function (e) {
                isAppsPull = true;
                LoadApps(function (result) {
                    CookieService.setCookie('apps-page-number', 1);
                    app.attachInfiniteScroll('#divInfiniteApps');
                    app.pullToRefreshDone();
                });
            });

            $$('#divInfiniteApps').on('infinite', function () {
                if (loadingApps) return;
                loadingApps = true;

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

                CookieService.setCookie('apps-page-number', parseInt(CookieService.getCookie('apps-page-number')) + 1);

                var appsParamter = {
                    'page_size': fw7.PageSize,
                    'page': parseInt(CookieService.getCookie('apps-page-number'))
                };

                appServices.CallService('apps', 'POST', "api/v1/applications", appsParamter, function (apps) {
                    SpinnerPlugin.activityStop();
                    if (apps.data && apps.data.length > 0) {
                        loadingApps = false;

                        angular.forEach(apps.data, function (app) {
                            allApps.push(app);
                        });

                        if (apps.data && apps.data.length <= fw7.PageSize) {
                            app.detachInfiniteScroll('#divInfiniteApps');
                            return;
                        }

                        $scope.apps = allApps;
                    }
                    else {
                        app.detachInfiniteScroll('#divInfiniteApps');
                        loadingApps = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });

        });

        app.onPageBeforeAnimation('apps', function (page) {
            if ($rootScope.currentOpeningPage != 'apps') return;
            $rootScope.currentOpeningPage = 'apps';

            isAppsPull = false;
            $scope.isPageLoaded = false;
            CookieService.setCookie('apps-page-number', 1);

            LoadApps(function (result) {
                $scope.isPageLoaded = true;
            });


            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('apps', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;
            CookieService.setCookie('apps-page-number', 1);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('apps', function (page) {
            if ($rootScope.currentOpeningPage != 'apps') return;
            $rootScope.currentOpeningPage = 'apps';

        });

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        $scope.OpenAppLink = function (app) {
            var website = app.app_link;
            if (typeof website != 'undefined' && website != null && (website).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(website, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('نجاح', 'رابط التطبيق غير صحيح .', 'alert', function () { });
            }

        };

        app.init();
    });

}]);

