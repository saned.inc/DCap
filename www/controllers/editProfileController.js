﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('editProfileController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    function LoadUserData(user) {
        $scope.editUserProfileForm.username = typeof user != 'undefined' && user.username != null ? user.username : '';
        $scope.editUserProfileForm.name = typeof user != 'undefined' && user.name != null ? user.name : '';

        var userCity = $scope.cites.filter(el => {
            if (el.id === user.city_id) {
                return true;
            }
        })[0];

        $scope.editUserProfileForm.city = user.city;
        var cityName = typeof user.city != 'undefined' && user.city != null ? user.city.name : 'المدينة';
        $('#divItemAfterUserCity').html(cityName);

        if (typeof user.image != 'undefined' && user.image != null && user.image != '' && user.image != ' ') {
            $scope.userImage = user.image;
        }
        else {
            $scope.userImage = 'http://placehold.it/100?text=image';
        }
    }

    function LoadAgentData(user) {
        $scope.editAgentProfileForm.username = typeof user != 'undefined' && user != null ? user.username : false;
        $scope.editAgentProfileForm.name = typeof user != 'undefined' && user.username != null ? user.username : '';
        $scope.editAgentProfileForm.whatsapp = typeof user != 'undefined' && user.whatsapp != null ? user.whatsapp : '';
        $scope.editAgentProfileForm.facebook = typeof user != 'undefined' && user.facebook != null ? user.facebook : '';
        $scope.editAgentProfileForm.google = typeof user != 'undefined' && user.google != null ? user.google : '';
        $scope.editAgentProfileForm.twitter = typeof user != 'undefined' && user.twitter != null ? user.twitter : '';
        $scope.editAgentProfileForm.instagram = typeof user != 'undefined' && user.instagram != null ? user.instagram : '';
        $scope.editAgentProfileForm.description = typeof user != 'undefined' && user.description != null ? user.description : '';

        $scope.editAgentProfileForm.city = user.city;

        var cityName = typeof user.city != 'undefined' && user.city != null ? user.city.name : 'المدينة';
        $('#divItemAfterAgentCity').html(cityName);

        if (typeof user.image != 'undefined' && user.image != null && user.image != '' && user.image != ' ') {
            $scope.agentImage = user.image;
        }
        else {
            $scope.agentImage = 'http://placehold.it/100?text=image';
        }

        if (typeof user.national_image != 'undefined' && user.national_image != null && user.national_image != '' && user.national_image != ' ') {
            $scope.nationalImage = user.national_image;
        }
        else {
            $scope.nationalImage = 'http://placehold.it/250x100?text=image';
        }

        $scope.categories = user.categories;

        var allAgentSubCategories = [];
        var categoriesNames = '';
        var subCategoriesNames = '';
        angular.forEach(user.categories, function (category, index) {
            categoriesNames = categoriesNames != '' ? ' , ' + category.name : category.name;
            angular.forEach(category.subCategories, function (subCategory, index) {
                allAgentSubCategories.push(subCategory);
                subCategoriesNames = subCategoriesNames != '' ? ' , ' + subCategory.name : subCategory.name;
            });
        });

        $scope.subCategories = allAgentSubCategories;
        $scope.editAgentProfileForm.category = user.categories;
        $scope.editAgentProfileForm.subCategory = allAgentSubCategories;
        $('#divItemAfterAgentCategories').html(categoriesNames);
        $('#divItemAfterAgentSubCategories').html(subCategoriesNames);
    }

    function LoadCategoriesAndSubs(callBack) {
        var categories = [];
        var subCategories = [];

        appServices.CallService('editProfile', "GET", "api/v1/categories", '', function (res) {
            SpinnerPlugin.activityStop();
            if (res != null && res.length > 0) {

                angular.forEach(res, function (category, index) {
                    category.index = index;
                    $scope.maxNum = parseInt(category.category_number);

                    angular.forEach(category.children, function (subCategory, index) {
                        subCategory.index = index;
                        subCategories.push(subCategory);
                    });
                });
                $scope.allCategories = res;
                $scope.subCategories = subCategories;
            }

            callBack(categories);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $document.ready(function () {
        app.onPageInit('editProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'editProfile') return;
            $rootScope.currentOpeningPage = 'editProfile';

        });

        app.onPageBeforeAnimation('editProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'editProfile') return;
            $rootScope.currentOpeningPage = 'editProfile';
            var userType = page.query.userType;
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            $scope.cites = myApp.fw7.Cities;

            if (userType == 'seeker') {
                $scope.resetUserForm();
                LoadUserData(userLoggedIn);
                $scope.IsSeeker = true;
            }
            else {
                $scope.resetAgentForm();
                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                LoadCategoriesAndSubs(function (result) {
                    LoadAgentData(userLoggedIn);
                });
                $scope.IsSeeker = false;
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('editProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'editProfile') return;
            $rootScope.currentOpeningPage = 'editProfile';

        });

        $scope.resetUserForm = function () {
            $scope.editUserProfileReset = false;
            $scope.editUserProfileForm.username = null;
            $scope.editUserProfileForm.name = null;
            $scope.editUserProfileForm.city = null;
            if (typeof $scope.EditUserProfileForm != 'undefined' && $scope.EditUserProfileForm != null) {
                $scope.EditUserProfileForm.$setPristine(true);
                $scope.EditUserProfileForm.$setUntouched();
            }
        }

        $scope.resetAgentForm = function () {
            $scope.editAgentProfileReset = false;
            $scope.editAgentProfileForm.username = null;
            $scope.editAgentProfileForm.name = null;
            $scope.editAgentProfileForm.city = null;
            $scope.editAgentProfileForm.whatsapp = null;
            $scope.editAgentProfileForm.facebook = null;
            $scope.editAgentProfileForm.google = null;
            $scope.editAgentProfileForm.twitter = null;
            $scope.editAgentProfileForm.instagram = null;
            $scope.editAgentProfileForm.description = null;
            if (typeof $scope.EditAgentProfileForm != 'undefined' && $scope.EditAgentProfileForm != null) {
                $scope.EditAgentProfileForm.$setPristine(true);
                $scope.EditAgentProfileForm.$setUntouched();
            }
        }

        function getImage(IsAgent, UploadNationalImage) {
            if (IsAgent && !UploadNationalImage) {
                navigator.camera.getPicture(uploadAgentPhoto, function (message) {

                }, {
                    quality: 100,
                    destinationType: navigator.camera.DestinationType.DATA_URL,
                    sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.JPEG,
                    //targetWidth: 300,
                    //targetHeight: 300,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false,
                    correctOrientation: true
                });
            }
            else if (IsAgent && UploadNationalImage) {
                navigator.camera.getPicture(uploadAgentNationalPhoto, function (message) {

                }, {
                    quality: 100,
                    destinationType: navigator.camera.DestinationType.DATA_URL,
                    sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.JPEG,
                    //targetWidth: 300,
                    //targetHeight: 300,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false,
                    correctOrientation: true
                });
            }
            else {
                navigator.camera.getPicture(uploadUserPhoto, function (message) {

                }, {
                    quality: 100,
                    destinationType: navigator.camera.DestinationType.DATA_URL,
                    sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: false,
                    encodingType: Camera.EncodingType.JPEG,
                    //targetWidth: 300,
                    //targetHeight: 300,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false,
                    correctOrientation: true
                });
            }

        }

        $scope.AddUserImage = function () {
            getImage(false,false);
        };

        $scope.AddAgentImage = function (IsNationalImage) {
            getImage(true, IsNationalImage);
        };

        function uploadAgentPhoto(imageURI) {
            $scope.agentImage = imageURI;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        function uploadAgentNationalPhoto(imageURI) {
            $scope.nationalImage = imageURI;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        function uploadUserPhoto(imageURI) {
            $scope.userImage = imageURI;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.FilterSubCategories = function (categories) {
            var subCategories = [];

            angular.forEach(categories, function (category, index) {
                angular.forEach(category.children, function (subCategory, index) {
                    subCategories.push(subCategory);
                });
            });

            $scope.subCategories = subCategories;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.CountSelectedSubs = function (SubCategories) {
            $scope.maxCategoriesExceeded = $scope.maxNum < SubCategories.length ? true : false;

            if ($scope.maxNum <= SubCategories.length) {
                $$('div[data-select-name="subCategory"] ul li').each(function (element, index) {
                    $(this).addClass('disabled');
                });
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        };

        $scope.submitUserForm = function (isValid) {
            $scope.editUserProfileReset = true;
            $scope.isUserImageEmpty = typeof $scope.userImage == "undefined" || $scope.userImage == null || $scope.userImage == '' || $scope.userImage == ' ' ? true : false;

            if (isValid && $scope.isUserImageEmpty == false) {
                var subCategories = [];

                subCategories = JSON.stringify(subCategories);

                var userImage = '';

                if ($scope.userImage == 'undefined' || $scope.userImage == null || $scope.userImage.indexOf('.jpg') == -1) {
                    userImage = $scope.userImage;
                }

                var user = {
                    'name': $scope.editUserProfileForm.name,
                    'username': $scope.editUserProfileForm.username,
                    'city_id': $scope.editUserProfileForm.city.id,
                    'categories': subCategories,
                    'email': 'fff@fff.com',
                    'image': userImage
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('editProfile', "POST", "api/v1/profile/update", user, function (res2) {
                    SpinnerPlugin.activityStop();
                    if (res2 != null) {
                        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                        userLoggedIn.name = $scope.editUserProfileForm.name;
                        userLoggedIn.username = $scope.editUserProfileForm.username;
                        userLoggedIn.city = $scope.editUserProfileForm.city;
                        userLoggedIn.image = res2.image;
                        CookieService.setCookie('userLoggedIn', JSON.stringify(userLoggedIn));

                        $scope.resetUserForm();
                        language.openFrameworkModal('نجاح', 'تم تعديل بياناتك بنجاح .', 'alert', function () {
                            helpers.GoToPage('userProfile', null);
                        });
                    }
                });
            }
        };

        $scope.submitAgentForm = function (isValid) {
            $scope.editAgentProfileReset = true;
            $scope.isAgentImageEmpty = typeof $scope.agentImage == "undefined" || $scope.agentImage == null || $scope.agentImage == '' || $scope.agentImage == ' ' ? true : false;
            $scope.isAgentNationalImageEmpty = typeof $scope.nationalImage == "undefined" || $scope.nationalImage == null || $scope.nationalImage == '' || $scope.nationalImage == ' ' ? true : false;

            if (isValid && $scope.isAgentImageEmpty == false && $scope.isAgentNationalImageEmpty == false) {
                var subCategories = [];
                angular.forEach($scope.editAgentProfileForm.subCategory, function (subCategory, index) {
                    subCategories.push(subCategory.id);
                });

                subCategories = JSON.stringify(subCategories);

                var agentImage = '';
                var nationalImage = '';

                if ($scope.agentImage == 'undefined' || $scope.agentImage == null || $scope.agentImage.indexOf('.jpg') == -1) {
                    agentImage = $scope.agentImage;
                }
                if ($scope.nationalImage == 'undefined' || $scope.nationalImage == null || $scope.nationalImage.indexOf('.jpg') == -1) {
                    nationalImage = $scope.nationalImage;
                }

                var user = {
                    'name': $scope.editAgentProfileForm.name,
                    'username': $scope.editAgentProfileForm.username,
                    'city_id': $scope.editAgentProfileForm.city.id,
                    'categories': subCategories,
                    'whatsapp': $scope.editAgentProfileForm.whatsapp,
                    'facebook': $scope.editAgentProfileForm.facebook,
                    'google': $scope.editAgentProfileForm.google,
                    'twitter': $scope.editAgentProfileForm.twitter,
                    'instagram': $scope.editAgentProfileForm.instagram,
                    'description': $scope.editAgentProfileForm.description,
                    'email': 'fff@fff.com',
                    'image': agentImage,
                    'national_image': nationalImage,
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('editProfile', "POST", "api/v1/profile/update", user, function (res2) {
                    SpinnerPlugin.activityStop();
                    if (res2 != null) {

                        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));
                        userLoggedIn.name = $scope.editAgentProfileForm.name;
                        userLoggedIn.username = $scope.editAgentProfileForm.username;
                        userLoggedIn.whatsapp = $scope.editAgentProfileForm.whatsapp;
                        userLoggedIn.facebook = $scope.editAgentProfileForm.facebook;
                        userLoggedIn.google = $scope.editAgentProfileForm.google;
                        userLoggedIn.twitter = $scope.editAgentProfileForm.twitter;
                        userLoggedIn.instagram = $scope.editAgentProfileForm.instagram;
                        userLoggedIn.description = $scope.editAgentProfileForm.description;
                        userLoggedIn.image = res2.image;
                        userLoggedIn.national_image = res2.national_image;
                        CookieService.setCookie('userLoggedIn', JSON.stringify(userLoggedIn));

                        $scope.resetAgentForm();
                        language.openFrameworkModal('نجاح', 'تم تعديل بياناتك بنجاح .', 'alert', function () {
                            helpers.GoToPage('agentProfile', null);
                        });
                    }
                });
            }
        };

        $scope.form = {};
        $scope.editUserProfileForm = {};
        $scope.editAgentProfileForm = {};


        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        app.init();
    });

}]);

