﻿myApp.angular.factory('CookieService', [function () {
    'use strict';

    var getCookie = function getCookie(cookieName) {
        return localStorage.getItem('DCap_' + cookieName) != "undefined" ? localStorage.getItem('DCap_' + cookieName) : null;
    }

    var setCookie = function setCookie(cookieName, cookieValue) {
        localStorage.setItem('DCap_' + cookieName, cookieValue);
    }

    var removeCookie = function (cookieName) {
        localStorage.removeItem('DCap_' + cookieName);
    }

    return {
        getCookie: getCookie,
        setCookie: setCookie,
        removeCookie: removeCookie
    };
}]);