﻿/// <reference path="../js/angular.js" />

(function () {

    myApp.angular.factory('appServices', ['$http', '$rootScope', '$log', 'helpers', 'CookieService', '$exceptionHandler', '$interval', function ($http, $rootScope, $log, helpers, CookieService, $exceptionHandler, $interval) {
        'use strict';

        var CallService = function (pageName, CallType, MethodName, dataVariables, callBack) {
            var counter = 0;
            var options = { dimBackground: true };

            if (!$rootScope.load) {
                $rootScope.load = false;
                if ((pageName == 'home' && MethodName.indexOf('api/Country/GetCountries') > -1) || pageName != 'home') {
                    //SpinnerPlugin.activityStart("تحميل ...", options);
                }
                else {
                    $rootScope.load = false;
                }
            }

            if (messageInterval) {
                helpers.clearTimer();
            }

            
            if (MethodName.indexOf('api/v1/register') == -1 && MethodName.indexOf('api/v1/profile/update') == -1) {
                messageInterval = $interval(function () {
                    counter++;
                    if (counter == 120) {
                        helpers.clearTimer();
                        language.openFrameworkModal('تنبيه', 'الخدمة غير متوفرة الآن بسبب بطء الإتصال بالإنترنت,من فضلك أعد المحاولة مرة أخري .', 'alert', function () {
                            callBack(null);
                        });
                    }
                }, 1000);
            }

            var contentType = "application/json";
            var token = CookieService.getCookie('appToken');
            var deviceId = CookieService.getCookie('deviceId');

            if (MethodName.indexOf('api/v1/agents/rates') > -1) {
                if (MethodName.indexOf('?') > -1) {
                    MethodName += '&&api_token=' + token;
                }
                else {
                    MethodName += '?api_token=' + token;
                }
                
                //contentType = "application/x-www-form-urlencoded";
            }

            if (dataVariables != '' && dataVariables != null) {
                if (typeof deviceId != 'undefined' && deviceId != null) {
                    dataVariables.deviceId = deviceId;
                }

                if (typeof token != 'undefined' && token != null && MethodName.indexOf('api/v1/login') == -1) {
                    dataVariables.api_token = token;
                }

                dataVariables = JSON.stringify(dataVariables);
            }
            else {
                if (typeof deviceId != 'undefined' && deviceId != null) {
                    dataVariables = { 'deviceId': deviceId };
                }
                if (typeof token != 'undefined' && token != null && MethodName.indexOf('api/v1/login') == -1) {
                    dataVariables = { 'api_token': token };
                }
                dataVariables = dataVariables;
            }

            $http({
                method: CallType,
                url: serviceURL + MethodName,
                headers: {
                    "content-type": contentType,
                    "cache-control": "no-cache"
                },
                data: dataVariables,
                async: true,
                crossDomain: true,
            }).then(
            function successCallback(response) {
                helpers.clearTimer();
                if (typeof response.data.status != 'undefined' && response.data.status != null) {
                    if (response.data.status == true || response.data.status == 'true' || response.data.status == 'success') {
                        if (typeof response.data.data != 'undefined' && response.data.data != null) {
                            callBack(response.data.data);
                        }
                        else if (typeof response.data != 'undefined' && response.data != null && typeof response.data.message != 'undefined' && response.data.message != null) {
                            callBack(response.data.message);
                        }
                        else if (typeof response.data.message.email != 'undefined' && response.data.message.email != null && response.data.message.email != '' && response.data.message.email.length > 0) {
                            language.openFrameworkModal('خطأ', response.data.message.email[0], 'alert', function () { });
                            callBack(true);
                        }
                        else {
                            if (response.data.message == 'Reset Code is invalid.') {
                                language.openFrameworkModal('خطأ', 'الكود غير صحيح', 'alert', function () { });
                            }
                            else if (response.data.message == 'Old Password is incorrect') {
                                language.openFrameworkModal('خطأ', 'كلمة السر القديمة غير صحيحة', 'alert', function () { });
                            }
                            else {
                                language.openFrameworkModal('خطأ', response.data.message, 'alert', function () { });
                            }
                            callBack(true);
                        }
                    }
                    else {
                        var message = response.data.message;
                        var errors = response.data.errors;

                        if (typeof response.data.message.email != 'undefined' && response.data.message.email != null && response.data.message.email != '' && response.data.message.email.length > 0) {
                            language.openFrameworkModal('خطأ', response.data.message.email[0], 'alert', function () { });
                            callBack(null);
                        }
                        else if (message == 'Old Password is incorrect') {
                            language.openFrameworkModal('خطأ', 'كلمة السر القديمة غير صحيحة', 'alert', function () { });
                            callBack(null);
                        }
                        else if (typeof message != 'undefined' && message != null) {
                            if (message == 'sorry, password not matched.') {
                                callBack('passwordWrong');
                            }
                            else {
                                language.openFrameworkModal('خطأ', message, 'alert', function () { });
                                callBack(null);
                            } 
                        }
                        else if (typeof errors != 'undefined' && errors != null && errors != '' && errors != ' ') {
                            var errorsArray = JSON.parse(errors);
                            callBack(null);
                        }
                        
                    }
                }
                else {
                    language.openFrameworkModal('خطأ', 'Error In Service', 'alert', function () { });
                    callBack(null);
                }
            },
            function errorCallback(error) {
                var error = error;
                if (error.status == 401) {
                    var message = error.data.data;
                    if (typeof error.data.message != 'undefined' && error.data.message != null) {
                        if (error.data.message == 'Sorry, Something is wrong.' || error.data.message == 'username or password is incorrect.' || error.data.message =='sorry, password not matched.') {
                            language.openFrameworkModal('خطأ', 'رقم الجوال أو كلمة السر خاطئين', 'alert', function () { });
                        }
                        else {
                            language.openFrameworkModal('خطأ', error.data.message, 'alert', function () { });
                        }
                    }
                    else {
                        if (typeof message != 'undefined' && message != null) {
                            language.openFrameworkModal('خطأ', message, 'alert', function () { });
                        }
                    }
                }
                else if (error.status == 500) {
                    language.openFrameworkModal('خطأ', 'Internal Server Error , Please Contact Application Support Team', 'alert', function () { });
                }
                else if (error.status == 400) {
                    var message = error.data.message;
                    if (typeof message != 'undefined' && message != null) {
                        if (message == 'your phone is not correct.') {
                            language.openFrameworkModal('خطأ', 'رقم الجوال الذي أدخلته غير صحيح .', 'alert', function () { });
                        }
                        else {
                            language.openFrameworkModal('خطأ', message, 'alert', function () { });
                        }
                    }
                }
                callBack(null);
            });
        }

        return {
            CallService: CallService
        }
    }]);

}());

    
