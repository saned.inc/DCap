﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('myRatingsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    function LoadAgentRates(callBack) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

        appServices.CallService('myRatings', "GET", "api/v1/agents/rates?agentId=" + userLoggedIn.id, '', function (res) {
            SpinnerPlugin.activityStop();

            if (res != null) {
                var agentRatings = res.ratings;
                var userRatedCount = typeof res.ratings != 'undefined' && res.ratings != null ? res.ratings.length : 0;
                var totalRates = 0;
                var sumOfAllRates = 0;
                var agentOverAllRate = 0;
                var FiveStarsRate = 0, FourStarsRate = 0, ThreeStarsRate = 0, TwoStarsRate = 0, OneStarsRate = 0;
                var FiveStarsRateProgress = 0, FourStarsRateProgress = 0, ThreeStarsRateProgress = 0, TwoStarsRateProgress = 0, OneStarsRateProgress = 0;
                var usersRated = res.userRated;

                angular.forEach(usersRated, function (userRated, index) {
                    userRated.name = typeof userRated.name != 'undefined' && userRated.name != null && userRated.name != '' && userRated.name != ' ' ? userRated.name : 'غير معرف';
                });

                angular.forEach(agentRatings, function (agentRate, index) {
                    var rate = typeof agentRate.rating != 'undefined' && agentRate.rating != null ? parseInt(agentRate.rating) : 0;
                    sumOfAllRates += parseInt(rate);
                    if (rate > 0) {
                        if (rate == 5) FiveStarsRate++;
                        else if (rate == 4) FourStarsRate++;
                        else if (rate == 3) ThreeStarsRate++;
                        else if (rate == 2) TwoStarsRate++;
                        else if (rate == 1) OneStarsRate++;
                        totalRates++;
                    }
                });

                agentOverAllRate = parseInt(sumOfAllRates) > 0 ? parseInt(sumOfAllRates) / parseInt(totalRates) : 0;
                agentOverAllRate = agentOverAllRate > 5 ? 5 : parseInt(agentOverAllRate);

                $scope.agentOverAllRate = agentOverAllRate + '.0';

                FiveStarsRateProgress = totalRates > 0 ? parseInt((FiveStarsRate / totalRates) * 100) : 0;
                FourStarsRateProgress = totalRates > 0 ? parseInt((FourStarsRate / totalRates) * 100) : 0;
                ThreeStarsRateProgress = totalRates > 0 ? parseInt((ThreeStarsRate / totalRates) * 100) : 0;
                TwoStarsRateProgress = totalRates > 0 ? parseInt((TwoStarsRate / totalRates) * 100) : 0;
                OneStarsRateProgress = totalRates > 0 ? parseInt((OneStarsRate / totalRates) * 100) : 0;

                $scope.IsRatingsExists = userRatedCount > 0 ? true : false;
                $scope.usersRated = usersRated;
                $scope.FiveStarsRate = FiveStarsRate;
                $scope.FourStarsRate = FourStarsRate;
                $scope.ThreeStarsRate = ThreeStarsRate;
                $scope.TwoStarsRate = TwoStarsRate;
                $scope.OneStarsRate = OneStarsRate;

                app.setProgressbar('#fiveStarProgressBar', FiveStarsRateProgress, 1000);
                app.setProgressbar('#fourStarProgressBar', FourStarsRateProgress, 1000);
                app.setProgressbar('#threeStarProgressBar', ThreeStarsRateProgress, 1000);
                app.setProgressbar('#twoStarProgressBar', TwoStarsRateProgress, 1000);
                app.setProgressbar('#oneStarProgressBar', OneStarsRateProgress, 1000);
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

            callBack(true);


        });
    }

    $document.ready(function () {
        app.onPageInit('myRatings', function (page) {
            if ($rootScope.currentOpeningPage != 'myRatings') return;
            $rootScope.currentOpeningPage = 'myRatings';

        });

        app.onPageBeforeAnimation('myRatings', function (page) {
            if ($rootScope.currentOpeningPage != 'myRatings') return;
            $rootScope.currentOpeningPage = 'myRatings';

            $scope.agentOverAllRate = 0;
            app.setProgressbar('#fiveStarProgressBar', 0, 100);
            app.setProgressbar('#fourStarProgressBar', 0, 100);
            app.setProgressbar('#threeStarProgressBar', 0, 100);
            app.setProgressbar('#twoStarProgressBar', 0, 100);
            app.setProgressbar('#oneStarProgressBar', 0, 100);
            $scope.usersRated = [];
            $scope.FiveStarsRate = 0;
            $scope.FourStarsRate = 0;
            $scope.ThreeStarsRate = 0;
            $scope.TwoStarsRate = 0;
            $scope.OneStarsRate = 0;

            LoadAgentRates(function () {

            });

            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('myRatings', function (page) {
            if ($rootScope.currentOpeningPage != 'myRatings') return;
            $rootScope.currentOpeningPage = 'myRatings';

        });

        app.onPageAfterAnimation('myRatings', function (page) {
            if ($rootScope.currentOpeningPage != 'myRatings') return;
            $rootScope.currentOpeningPage = 'myRatings';

        });

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        app.init();
    });

}]);

