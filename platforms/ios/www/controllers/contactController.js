﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('contactController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    function LoadContactUsInfo(callBack) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

        appServices.CallService('about', "GET", "api/v1/contactinfo", '', function (res) {
            SpinnerPlugin.activityStop();
            if (res != null) {
                $scope.contactInfo = res;
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $document.ready(function () {
        app.onPageInit('contact', function (page) {
            if ($rootScope.currentOpeningPage != 'contact') return;
            $rootScope.currentOpeningPage = 'contact';

            LoadContactUsInfo(function (result) {
                $scope.selectedItem = $rootScope.currentUserCity;
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageBeforeAnimation('contact', function (page) {
            if ($rootScope.currentOpeningPage != 'contact') return;
            $rootScope.currentOpeningPage = 'contact';
            $scope.resetForm();

            var userLoggedIn = CookieService.getCookie('userLoggedIn') ? CookieService.getCookie('userLoggedIn') : null;
            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                $$('#divContactUs').show();
            }
            else {
                $$('#divContactUs').hide();
            }

            LoadContactUsInfo(function (result) {

            });


            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('contact', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });


        app.onPageAfterAnimation('contact', function (page) {
            if ($rootScope.currentOpeningPage != 'contact') return;
            $rootScope.currentOpeningPage = 'contact';
            
        });

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        $scope.resetForm = function () {
            $scope.contactUsReset = false;
            $scope.contactUsForm.name = null;
            $scope.contactUsForm.mobile = null;
            $scope.contactUsForm.email = null;
            $scope.contactUsForm.message = null;
            if (typeof $scope.ContactUsForm != 'undefined' && $scope.ContactUsForm != null) {
                $scope.ContactUsForm.$setPristine(true);
                $scope.ContactUsForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }

        $scope.submitForm = function (isValid) {
            $scope.contactUsReset = true;
            if (isValid) {
                var params = {
                    'name': $scope.contactUsForm.name,
                    'phone': $scope.contactUsForm.mobile,
                    'email': $scope.contactUsForm.email,
                    'message': $scope.contactUsForm.message
                };

                var isAndroid = Framework7.prototype.device.android === true;

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('contact', "POST", "api/v1/contactus", params, function (res2) {
                    SpinnerPlugin.activityStop();
                    if (res2 != null) {
                        language.openFrameworkModal('نجاح', 'تم إرسال رسالتك لإدارة التطبيق بنجاح .', 'alert', function () {
                            $scope.resetForm();
                            if (isAndroid) {
                                helpers.GoToPage('homeIos', null);
                            }
                            else {
                                helpers.GoToPage('homeIos', null);
                            }
                        });
                    }
                });
            }
        };

        $scope.form = {};
        $scope.contactUsForm = {};

        $scope.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        $scope.nameRegex = /^[A-Za-z0-9]*$/;
        $scope.mobileRegex = /^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/;

        $scope.OpenSocial = function (url) {
            if ((url).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                cordova.InAppBrowser.open(url, '_system', 'location=no');
            }
            else {
                language.openFrameworkModal('نجاح', 'رابط التواصل الإجتماعي غير صحيح .', 'alert', function () { });
            }
        };
      
        app.init();
    });

}]);

