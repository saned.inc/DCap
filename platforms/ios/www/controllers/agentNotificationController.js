﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('agentNotificationController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var isNotificationsPull = true;
    var loadingNotifications = false;
    var allAgentNotifications = [];

    function LoadNotifications(callBack) {
        if (!isNotificationsPull) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        }

        allAgentNotifications = [];
        app.attachInfiniteScroll('#divInfiniteAgentNotifications');

        var params = {
            'page_size': fw7.PageSize,
            'page': 0
        };

        appServices.CallService('agentNotification', "POST", "api/v1/notifications", params, function (res) {
            if (!isNotificationsPull) {
                SpinnerPlugin.activityStop();
            }
            if (res.data != null && res.data.length > 0) {
                $$('.page[data-page="agentNotification"]').removeClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'relative');

                angular.forEach(res.data, function (notification) {
                    notification.color = notification.data.color;
                    notification.icon = notification.data.icon;
                    notification.text = notification.data.text;

                    allAgentNotifications.push(notification);
                });

                $scope.Notifications = allAgentNotifications;
            }
            else {
                $$('.page[data-page="agentNotification"]').addClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'absolute');
            }

            callBack(allAgentNotifications);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }


    $document.ready(function () {
        app.onPageInit('agentNotification', function (page) {
            if ($rootScope.currentOpeningPage != 'agentNotification') return;
            $rootScope.currentOpeningPage = 'agentNotification';


            $$('#divInfiniteAgentNotifications').on('ptr:refresh', function (e) {               
                isNotificationsPull = true;
                CookieService.setCookie('agentNotifications-page-number', 1);
                app.attachInfiniteScroll('#divInfiniteAgentNotifications');
                LoadNotifications(function (result) {
                    app.pullToRefreshDone();
                });
            });

            $$('#divInfiniteAgentNotifications').on('infinite', function () {
                if (loadingNotifications) return;
                loadingNotifications = true;

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

                CookieService.setCookie('agentNotifications-page-number', parseInt(CookieService.getCookie('agentNotifications-page-number')) + 1);

                var notificationsParamter = {
                    'page_size': fw7.PageSize,
                    'page': parseInt(CookieService.getCookie('agentNotifications-page-number'))
                };

                appServices.CallService('agentNotification', 'POST', "api/v1/notifications", notificationsParamter, function (notifications) {
                    SpinnerPlugin.activityStop();
                    if (notifications.data && notifications.data.length > 0) {
                        loadingNotifications = false;

                        angular.forEach(notifications.data, function (notification) {
                            notification.color = notification.data.color;
                            notification.icon = notification.data.icon;
                            notification.text = notification.data.text;

                            allAgentNotifications.push(notification);
                        });

                        if (notifications.data && notifications.data.length <= fw7.PageSize) {
                            app.detachInfiniteScroll('#divInfiniteAgentNotifications');
                            return;
                        }

                        $scope.Notifications = allAgentNotifications;
                    }
                    else {
                        app.detachInfiniteScroll('#divInfiniteAgentNotifications');
                        loadingNotifications = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });

        });

        app.onPageReinit('agentNotification', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;
            CookieService.setCookie('agentNotifications-page-number', 1);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageBeforeAnimation('agentNotification', function (page) {
            if ($rootScope.currentOpeningPage != 'agentNotification') return;
            $rootScope.currentOpeningPage = 'agentNotification';
            CookieService.setCookie('agentNotifications-page-number', 1);

            isNotificationsPull = false;
            $scope.isPageLoaded = false;

            LoadNotifications(function (result) {
                $scope.isPageLoaded = true;
            });


            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('agentNotification', function (page) {
            if ($rootScope.currentOpeningPage != 'agentNotification') return;
            $rootScope.currentOpeningPage = 'agentNotification';

        });

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        app.init();
    });

}]);

