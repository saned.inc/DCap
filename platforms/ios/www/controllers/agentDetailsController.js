﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('agentDetailsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    function GetRateText(rating) {
        var rateText=''
        if (rating == 1) {
            rateText = 'سيء';
        }
        else if (rating == 2) {
            rateText = 'مقبول';
        }
        else if (rating == 2) {
            rateText = 'جيد';
        }
        else if (rating == 2) {
            rateText = 'جيد جدا';
        }
        else if (rating == 2) {
            rateText = 'ممتاز';
        }

        return rateText;
    }

    function LoadAgentDetails(agentId,callBack) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        
        var IsVisitor = CookieService.getCookie('Visitor') ? CookieService.getCookie('Visitor') : false;

        appServices.CallService('agentDetails', "GET", "api/v1/user/" + agentId, '', function (res) {
            SpinnerPlugin.activityStop();

            if (res != null && res.length > 0) {
                var agentRatings = res[0].ratings;
                var userRatedCount = typeof res[0].ratings != 'undefined' && res[0].ratings != null ? res[0].ratings.length : 0;
                var totalRate = 0;
                var userRate = 0;
                var agentSubCategories = [];
                var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

                angular.forEach(agentRatings, function (agentRate, index) {
                    var rate = 0;
                    rate += parseInt(agentRate.rating);
                    totalRate = parseInt(rate / parseInt(userRatedCount));

                    if (typeof userLoggedIn != 'undefined' && userLoggedIn != null && parseInt(userLoggedIn.id) == parseInt(agentRate.user_id)) {
                        userRate = parseInt(agentRate.rating);
                    }
                });

                var allParents = [];

                angular.forEach(res[0].categories, function (category, index) {
                    var category = category;
                    var parent = category.parent;
                    var parentIndex = allParents.length > 0 ? allParents.findIndex(x => x.id == parent.id) : -1;

                    if (parentIndex == -1) {
                        allParents.push(parent);

                        angular.forEach(category.parent.children, function (agentSubCategory, index) {
                            agentSubCategories.push(agentSubCategory);
                        });
                    }
                });

                res[0].totalRate = totalRate;
                res[0].phone = typeof res[0].phone != 'undefined' && res[0].phone != null && res[0].phone != '' && res[0].phone != ' ' ? res[0].phone : ' ----- ';
                res[0].whatsapp = typeof res[0].whatsapp != 'undefined' && res[0].whatsapp != null && res[0].whatsapp != '' && res[0].whatsapp != ' ' ? res[0].whatsapp : ' ----- ';
                $scope.IsVisitor = IsVisitor == 'true' ? true : false;
                $scope.IsOwner = typeof userLoggedIn != 'undefined' && userLoggedIn != null && parseInt(userLoggedIn.id) == parseInt(res[0].id) ? true : false;
                $scope.noDescription = typeof res[0].description != 'undefined' && res[0].description != null && res[0].description != '' && res[0].description != ' ' ? false : true;
                $scope.noSubCategories = agentSubCategories.length > 0 ? false : true;
                $scope.userRate = userRate;
                $scope.rateText = GetRateText(totalRate);
                $scope.agentSubCategories = agentSubCategories;
                $scope.agent = res[0];
            }
            else {
                $scope.IsVisitor = true;
                $scope.IsOwner = false;
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $document.ready(function () {
        app.onPageInit('agentDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'agentDetails') return;
            $rootScope.currentOpeningPage = 'agentDetails';

        });

        app.onPageBeforeAnimation('agentDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'agentDetails') return;
            $rootScope.currentOpeningPage = 'agentDetails';

            var agentId = page.query.agentId;

            LoadAgentDetails(agentId, function () {

            });

            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);

        });

        app.onPageReinit('agentDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'agentDetails') return;
            $rootScope.currentOpeningPage = 'agentDetails';

        });

        app.onPageAfterAnimation('agentDetails', function (page) {
            if ($rootScope.currentOpeningPage != 'agentDetails') return;
            $rootScope.currentOpeningPage = 'agentDetails';

        });

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        $scope.ReportAgent = function (agent) {
            helpers.GoToPage('report', { agentId: agent.id, agentName: agent.name });
        };

        $scope.CallAgent = function (agent) {
            var contactPhone = agent.phone;

            window.plugins.CallNumber.callNumber(
              function onSuccess(successResult) {

              }, function onError(errorResult) {
                  console.log("Error:" + errorResult);
              }, contactPhone, true);
        };

        $scope.CallAgentByWhatsApp = function (whatsNumber) {
            var scheme;

            if (device.platform === 'iOS') {
                scheme = 'whatsapp://';
            }
            else if (device.platform === 'Android') {
                scheme = 'com.whatsapp';
            }

            appAvailability.check(
                scheme,
                function () {
                    if (typeof whatsNumber != 'undefined' && whatsNumber != null) {
                        cordova.InAppBrowser.open('whatsapp://send?=&phone=+' + whatsNumber, '_system');
                    }
                    else {
                        language.openFrameworkModal('نجاح', 'لا يوجد رقم واتس أب للمندوب .', 'alert', function () { });
                    }
                },
                function () {
                    language.openFrameworkModal('نجاح', 'من فضلك قم بتحميل تطبيق الواتس أب أولا .', 'alert', function () { });
                }
            );

            
        };

        $scope.OpenWebsite = function (website) {
            if (typeof website != 'undefined' && website != null) {
                if ((website).match(/(http:|https:)?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/)) {
                    cordova.InAppBrowser.open(website, '_system', 'location=no');
                }
                else {
                    language.openFrameworkModal('نجاح', 'الرابط غير صحيح .', 'alert', function () { });
                }
            }
            else {
                language.openFrameworkModal('نجاح', 'لا يوجد رابط .', 'alert', function () { });
            }
        };

        $scope.rateAgent = function (rating,agent) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

            var params = {
                'user_id': agent.id,
                'rate_value': rating
            };

            appServices.CallService('agentDetails', "POST", "api/v1/rating", params, function (res) {
                SpinnerPlugin.activityStop();
                if (res != null) {
                    language.openFrameworkModal('نجاح', 'تم تقييم المندوب بنجاح , شكرا لتفاعلك معنا .', 'alert', function () {
                        
                    });
                }
            });
        };

        app.init();
    });

}]);

