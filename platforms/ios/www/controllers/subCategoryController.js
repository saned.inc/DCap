﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('subCategoryController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var isSubCategoriesPull = true;
    var allSubCategories = [];
    var loadingSubCategories = false;
    var categoryId;

    function LoadSubCategories(subCategories, callBack) {
        app.pullToRefreshDone();

        if (!isSubCategoriesPull) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        }
        allSubCategories = JSON.parse(subCategories);

        if (typeof allSubCategories != 'undefined' && allSubCategories != null && allSubCategories != '' && allSubCategories != ' ' && allSubCategories.length > 0) {
            $$('.page[data-page="subCategory"]').removeClass('noResult');
            $$('.pull-to-refresh-layer').css('position', 'relative');
        }
        else {
            $$('.page[data-page="subCategory"]').addClass('noResult');
            $$('.pull-to-refresh-layer').css('position', 'absolute');
        }

        if (!isSubCategoriesPull) {
            SpinnerPlugin.activityStop();
        }

        $scope.SubCategories = allSubCategories;
        callBack(allSubCategories);

        setTimeout(function () {
            $scope.$apply();
        }, fw7.DelayBeforeScopeApply);
    }

    $document.ready(function () {
        app.onPageInit('subCategory', function (page) {
            if ($rootScope.currentOpeningPage != 'subCategory') return;
            $rootScope.currentOpeningPage = 'subCategory';
            var subCategories = page.query.subCategories;
            categoryId = page.query.categoryId;


            $$('#divInfiniteSubCategories').on('ptr:refresh', function (e) {
                isSubCategoriesPull = true;

                appServices.CallService('home', "GET", "api/v1/categories", '', function (res) {
                    if (res != null && res.length > 0) {
                        angular.forEach(res, function (category, index) {
                            if (category.id == parseInt(categoryId)) {
                                allSubCategories = category.children;
                            }
                        });
                    }

                    $scope.SubCategories = allSubCategories;

                    CookieService.setCookie('subCategory-page-number', 1);
                    app.attachInfiniteScroll('#divInfiniteSubCategories');
                    setTimeout(function () {
                        app.pullToRefreshDone();
                    }, 1000);

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });

        });

        app.onPageBeforeAnimation('subCategory', function (page) {
            if ($rootScope.currentOpeningPage != 'subCategory') return;
            $rootScope.currentOpeningPage = 'subCategory';

            var subCategories = page.query.subCategories;
            categoryId = page.query.categoryId;

            isSubCategoriesPull = false;
            $scope.isPageLoaded = false;

            LoadSubCategories(subCategories, function (result) {
                $scope.isPageLoaded = true;
            });

            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('subCategory', function (page) {
            if ($rootScope.currentOpeningPage != 'subCategory') return;
            $rootScope.currentOpeningPage = 'subCategory';
            categoryId = page.query.categoryId;
        });

        app.onPageAfterAnimation('subCategory', function (page) {
            if ($rootScope.currentOpeningPage != 'subCategory') return;
            $rootScope.currentOpeningPage = 'subCategory';
            categoryId = page.query.categoryId;
        });

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.GoToAgents = function (subCategory) {
            helpers.GoToPage('agents', { subCategoryId: subCategory.id });
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        app.init();
    });

}]);

