﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('aboutController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    function LoadAboutUsInfo(callBack) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

        appServices.CallService('about', "GET", "api/v1/aboutapp", '', function (res) {
            SpinnerPlugin.activityStop();
            if (res != null) {
                $scope.aboutInfo = res;
            }

            callBack(true);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $document.ready(function () {
        app.onPageInit('about', function (page) {
            if ($rootScope.currentOpeningPage != 'about') return;
            $rootScope.currentOpeningPage = 'about';

            LoadAboutUsInfo(function (result) {
                $scope.selectedItem = $rootScope.currentUserCity;
            });

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageBeforeAnimation('about', function (page) {
            if ($rootScope.currentOpeningPage != 'about') return;
            $rootScope.currentOpeningPage = 'about';

            LoadAboutUsInfo(function (result) {

            });


            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('about', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('about', function (page) {
            if ($rootScope.currentOpeningPage != 'about') return;
            $rootScope.currentOpeningPage = 'about';

        });

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        app.init();
    });

}]);

