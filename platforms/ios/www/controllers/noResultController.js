﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('noResultController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var fromPage = '';

    $document.ready(function () {
        app.onPageInit('noResult', function (page) {
            if ($rootScope.currentOpeningPage != 'noResult') return;
            $rootScope.currentOpeningPage = 'noResult';

        });

        app.onPageBeforeAnimation('noResult', function (page) {
            if ($rootScope.currentOpeningPage != 'noResult') return;
            $rootScope.currentOpeningPage = 'noResult';

            $scope.resetForm();
        });

        app.onPageReinit('noResult', function (page) {
            if ($rootScope.currentOpeningPage != 'noResult') return;
            $rootScope.currentOpeningPage = 'noResult';

            $scope.resetForm();
        });

        app.onPageAfterAnimation('noResult', function (page) {
            if ($rootScope.currentOpeningPage != 'noResult') return;
            $rootScope.currentOpeningPage = 'noResult';


            $scope.resetForm();

        });

        $scope.resetForm = function () {
            //$scope.activationForm.code = null;
            //if (typeof $scope.ActivationForm != 'undefined' && $scope.ActivationForm != null) {
            //    $scope.ActivationForm.$setPristine(true);
            //    $scope.ActivationForm.$setUntouched();
            //}
        }


        $scope.GoBack = function () {
            helpers.GoBack();
        }

        app.init();
    });

    $scope.GoToHome = function () {
        helpers.GoToPage('homeIos', null);
    };

    $scope.GoToSettings = function () {
        helpers.GoToPage('setting', null);
    };

    $scope.GoToSearch = function () {
        helpers.GoToPage('search', null);
    };

    $scope.GoToNotification = function () {
        helpers.GoToPage('notification', null);
    };

    $scope.GoToProfile = function () {
        helpers.GoToPage('profile', null);
    };

}]);

