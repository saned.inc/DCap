﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('agentsController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var isAgentsPull = true;
    var subCategoryId = '';
    var loadingAgents;
    var cityId = '';
    var agentName = '';
    var allAgents = [];
    var pageFrom;

    function LoadAgents(callBack) {
        app.pullToRefreshDone();

        if (!isAgentsPull) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        }
        allAgents = [];
        app.attachInfiniteScroll('#divInfiniteAgents');

        var params = {
            'city_id': cityId,
            'agent_name': agentName,
            'category_id': subCategoryId,
            'page_size': fw7.PageSize,
            'page': 0
        };

        appServices.CallService('agents', "POST", "api/v1/agents/categories", params, function (res) {
            if (!isAgentsPull) {
                SpinnerPlugin.activityStop();
            }

            if (res.data != null && res.data.length > 0) {
                $$('.page[data-page="agents"]').removeClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'relative');

                var users = res.data;

                angular.forEach(users, function (agent, index) {
                    var agentRatings = agent.ratings;
                    var userRatedCount = typeof agent.ratings != 'undefined' && agent.ratings != null ? agent.ratings.length : 0;
                    var totalRate = 0;

                    angular.forEach(agentRatings, function (agentRate, index) {
                        var rate = 0;
                        rate += parseInt(agentRate.rating);
                        totalRate = parseInt(rate / parseInt(userRatedCount));
                    });

                    agent.totalRate = totalRate;
                    agent.image = typeof agent.image != 'undefined' && agent.image != null && agent.image != '' && agent.image != ' ' ? agent.image : 'http://placehold.it/100?text=image';
                });
                allAgents = users;
                $scope.agents = users;
            }
            else {
                $$('.page[data-page="agents"]').addClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'absolute');
                allAgents = [];
                $scope.agents = [];
            }

            callBack(allAgents);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $document.ready(function () {
        app.onPageInit('agents', function (page) {
            if ($rootScope.currentOpeningPage != 'agents') return;
            $rootScope.currentOpeningPage = 'agents';

            var pageFrom = page.fromPage.name;

            if (pageFrom == 'search') {
                subCategoryId = page.query.subCategory;
                cityId = page.query.city;
                agentName = page.query.name;
            }
            else {
                subCategoryId = page.query.subCategoryId;
                cityId = $rootScope.cityId;
            }
            

            $$('#divInfiniteAgents').on('ptr:refresh', function (e) {
                isAgentsPull = true;

                LoadAgents(function (result) {
                    CookieService.setCookie('agents-page-number', 1);
                    app.attachInfiniteScroll('#divInfiniteAgents');
                    app.pullToRefreshDone();
                });
            });

            $$('#divInfiniteAgents').on('infinite', function () {
                if (loadingAgents) return;
                loadingAgents = true;

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

                CookieService.setCookie('agents-page-number', parseInt(CookieService.getCookie('agents-page-number')) + 1);

                var agentsParamter = {
                    'city_id': cityId,
                    'agent_name': agentName,
                    'category_id': subCategoryId,
                    'page_size': fw7.PageSize,
                    'page': parseInt(CookieService.getCookie('agents-page-number'))
                };

                appServices.CallService('agents', 'POST', "api/v1/agents/categories", agentsParamter, function (agents) {
                    SpinnerPlugin.activityStop();
                    if (agents.data && agents.data.length > 0) {
                        loadingAgents = false;

                        angular.forEach(agents.data, function (agent) {
                            var agentRatings = agent.ratings;
                            var userRatedCount = typeof agent.ratings != 'undefined' && agent.ratings != null ? agent.ratings.length : 0;
                            var totalRate = 0;

                            angular.forEach(agentRatings, function (agentRate, index) {
                                var rate = 0;
                                rate += parseInt(agentRate.rating);
                                totalRate = parseInt(rate / parseInt(userRatedCount));
                            });

                            agent.totalRate = totalRate;
                            agent.image = typeof agent.image != 'undefined' && agent.image != null && agent.image != '' && agent.image != ' ' ? agent.image : 'http://placehold.it/100?text=image';
                            allAgents.push(agent);
                        });

                        if (agents.data && agents.data.length <= fw7.PageSize) {
                            app.detachInfiniteScroll('#divInfiniteAgents');
                            return;
                        }

                        $scope.agents = allAgents;
                    }
                    else {
                        app.detachInfiniteScroll('#divInfiniteAgents');
                        loadingAgents = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });

        });

        app.onPageBeforeAnimation('agents', function (page) {
            if ($rootScope.currentOpeningPage != 'agents') return;
            $rootScope.currentOpeningPage = 'agents';

            pageFrom = page.fromPage.name;

            if (pageFrom == 'search') {
                subCategoryId = page.query.subCategory;
                cityId = page.query.city;
                agentName = page.query.name;
            }
            else {
                subCategoryId = page.query.subCategoryId;
                cityId = $rootScope.cityId;
            }

            isAgentsPull = false;
            $scope.isPageLoaded = false;
            CookieService.setCookie('agents-page-number', 1);

            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;
            cityId = typeof myApp.fw7.Cities != 'undefined' && myApp.fw7.Cities != null && myApp.fw7.Cities.length > 0 ? myApp.fw7.Cities[0].id : undefined;
            $rootScope.cityId = typeof myApp.fw7.Cities != 'undefined' && myApp.fw7.Cities != null && myApp.fw7.Cities.length > 0 ? myApp.fw7.Cities[0].id : undefined;

            LoadAgents(function (result) {
                $scope.isPageLoaded = true;
            });

            
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('agents', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;
            CookieService.setCookie('agents-page-number', 1);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('agents', function (page) {
            if ($rootScope.currentOpeningPage != 'agents') return;
            $rootScope.currentOpeningPage = 'agents';

        });

        $scope.GoToAgentDetails = function (agent) {
            helpers.GoToPage('agentDetails', { agentId: agent.id });
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            if (typeof selectedCity != 'undefined' && selectedCity != null) {
                $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
                $('#linkHomeBannerCity .item-after').html('');
                $rootScope.currentUserCity = selectedCity;
                $rootScope.cityId = selectedCity.id;
                cityId = $rootScope.cityId;
            }
            else {
                $('#linkHomeBannerCity .item-inner').html('اختر المدينة');
                $('#linkHomeBannerCity .item-after').html('');
                $rootScope.currentUserCity = null;
                $rootScope.cityId = undefined;
                cityId = undefined;
            }

            if (pageFrom == 'search') {
                subCategoryId = '';
                agentName = '';
            }

            LoadAgents(function (result) {
                app.pullToRefreshDone();
            });
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        app.init();
    });

}]);

