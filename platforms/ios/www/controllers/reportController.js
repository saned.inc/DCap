﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('reportController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var agentId;
    var userId;
    var agentName;

    $document.ready(function () {
        app.onPageInit('report', function (page) {
            if ($rootScope.currentOpeningPage != 'report') return;
            $rootScope.currentOpeningPage = 'report';

        });

        app.onPageBeforeAnimation('report', function (page) {
            if ($rootScope.currentOpeningPage != 'report') return;
            $rootScope.currentOpeningPage = 'report';
            var userLoggedIn = JSON.parse(CookieService.getCookie('userLoggedIn'));

            if (typeof userLoggedIn != 'undefined' && userLoggedIn != null) {
                userId = userLoggedIn.id;
            }

            agentId = page.query.agentId;
            agentName = page.query.agentName;

            $scope.resetForm();
            $scope.reportForm.agentName = agentName;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('report', function (page) {
            $scope.resetForm();
        });

        app.onPageAfterAnimation('report', function (page) {
            if ($rootScope.currentOpeningPage != 'report') return;
            $rootScope.currentOpeningPage = 'report';

            
        });

        $scope.resetForm = function () {
            $scope.reportReset = false;
            $scope.reportForm.agentName = null;
            $scope.reportForm.reportText = null;
            if (typeof $scope.ReportForm != 'undefined' && $scope.ReportForm != null) {
                $scope.ReportForm.$setPristine(true);
                $scope.ReportForm.$setUntouched();
            }
        }

        $scope.submitForm = function (isValid) {
            $scope.reportReset = true;
            if (isValid) {
                var params = {
                    'repoter_id': userId,
                    'reporter_about_id': agentId,
                    'report': $scope.reportForm.reportText
                };

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
                appServices.CallService('report', "POST", "api/v1/report", params, function (res2) {
                    SpinnerPlugin.activityStop();
                    if (res2 != null) {
                        language.openFrameworkModal('نجاح', 'تم تسجيل البلاغ بنجاح .', 'alert', function () {
                        });
                        helpers.GoBack();
                    }
                });
            }
        };

        $scope.form = {};
        $scope.reportForm = {};

        $scope.CancelReport = function () {
            $scope.resetForm();
            helpers.GoBack();
        };

        app.init();
    });

}]);

