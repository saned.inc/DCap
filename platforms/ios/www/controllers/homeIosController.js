﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('homeIosController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var isHomeIosSubCategoriesPull = true;

    function ClearHistory() {
        for (var i = 0; i < fw7.views[0].history.length; i++) {
            if (fw7.views[0].history[i] === '#landing' || fw7.views[0].history[i] === '#login') fw7.views[0].history.splice(i, 1);
        }
    }

    function LoadCities(callBack) {
        app.pullToRefreshDone();

        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        var cities = [];

        appServices.CallService('home', "GET", "api/v1/cities", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.cities = res;
                $rootScope.currentUserCity = $scope.cities[0];
                $scope.selectedItem = $rootScope.currentUserCity;
                $scope.CurrentUserCityName = $rootScope.currentUserCity.name;
                myApp.fw7.Cities = res;
            }

            callBack(cities);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        
    }

    function LoadCategories(callBack) {
        var categories = [];

        appServices.CallService('home', "GET", "api/v1/categories", '', function (res) {
            if (!isHomeIosSubCategoriesPull) {
                SpinnerPlugin.activityStop();
            }
            if (res != null && res.length > 0) {
                $$('.page[data-page="homeIos"]').removeClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'relative');

                angular.forEach(res, function (category, index) {
                    category.index = index;
                });
                $scope.Categories = res;
            }
            else {
                $$('.page[data-page="homeIos"]').addClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'absolute');
            }

            callBack(categories);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $document.ready(function () {
        app.onPageInit('homeIos', function (page) {
            if ($rootScope.currentOpeningPage != 'homeIos') return;
            $rootScope.currentOpeningPage = 'homeIos';

            $$('#divInfiniteHomeIos').on('ptr:refresh', function (e) {
                isHomeIosSubCategoriesPull = true;
                LoadCategories(function (result) {
                    app.pullToRefreshDone();
                });
            });

            ClearHistory();
            LoadCities(function (cities) {
                LoadCategories(function (result) {
                    $scope.selectedItem = $rootScope.currentUserCity;
                });
            });

            SidePanelService.DrawMenu();
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageBeforeAnimation('homeIos', function (page) {
            if ($rootScope.currentOpeningPage != 'homeIos') return;
            $rootScope.currentOpeningPage = 'homeIos';

            $scope.isPageLoaded = false;
            isHomeIosSubCategoriesPull = false;

            LoadCities(function (cities) {
                LoadCategories(function (result) {
                    $scope.isPageLoaded = true;
                    $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
                    $('#linkHomeBannerCity .item-after').html('');
                    $scope.selectedItem = $rootScope.currentUserCity;
                    $scope.cites = myApp.fw7.Cities;
                });
            });

           
            ClearHistory();
            SidePanelService.DrawMenu();
        });

        app.onPageReinit('homeIos', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('homeIos', function (page) {
            if ($rootScope.currentOpeningPage != 'homeIos') return;
            $rootScope.currentOpeningPage = 'homeIos';

            ClearHistory();
            SidePanelService.DrawMenu();
        });

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.GoToSubCategories = function (category) {
            helpers.GoToPage('subCategory', { subCategories: JSON.stringify(category.children), categoryId: category.id });
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        app.init();
    });

}]);

