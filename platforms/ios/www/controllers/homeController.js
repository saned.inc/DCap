﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('homeController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    
    var isPageReInit = false;
    var isPageInit = false;
    var isCategoriesPull = true;
    var currentTabIndex = 0;

    function ClearHistory() {
        for (var i = 0; i < fw7.views[0].history.length; i++) {
            if (fw7.views[0].history[i] === '#landing' || fw7.views[0].history[i] === '#login') fw7.views[0].history.splice(i, 1);
        }
    }

    function LoadCities(callBack) {
        app.pullToRefreshDone();

        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        var cities = [];

        appServices.CallService('home', "GET", "api/v1/cities", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.cities = res;
                $rootScope.currentUserCity = $scope.cities[0];
                $rootScope.cityId = $scope.cities[0].id;
                $scope.selectedItem = $rootScope.currentUserCity;
                $scope.CurrentUserCityName = $rootScope.currentUserCity.name;
                myApp.fw7.Cities = res;
            }

            callBack(cities);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function LoadCategories(callBack) {
        var categories = [];
        
        appServices.CallService('home', "GET", "api/v1/categories", '', function (res) {
            SpinnerPlugin.activityStop();
            if (res != null && res.length > 0) {
                $$('.page[data-page="home"]').removeClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'relative');

                angular.forEach(res, function (category,index) {
                    category.index = index;
                    category.noChildren = typeof category.children != 'undefined' && category.children != null && category.children.length > 0 ? false : true;
                });

                $scope.Categories = res;
            }
            else {
                $$('.page[data-page="home"]').addClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'absolute');
            }

            callBack(categories);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    function InitializeTabsWidth() {
        var divTabs = document.getElementById('divTabs');
        divTabs.style.width = (parseInt($scope.Categories.length) * 100) + '%';
    }

    $document.ready(function () {
        app.onPageInit('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;

            $$('#divInfiniteHome').on('ptr:refresh', function (e) {
                isCategoriesPull = true;
                LoadCategories(function (result) {
                    InitializeTabsWidth();
                    $scope.selectedItem = $rootScope.currentUserCity;
                    $scope.currentTabIndex = currentTabIndex;
                    //app.pullToRefreshDone();

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });
            
            ClearHistory();
            LoadCities(function (cities) {
                LoadCategories(function (result) {
                    InitializeTabsWidth();
                    $scope.selectedItem = $rootScope.currentUserCity;
                    currentTabIndex = 0;
                    $scope.currentTabIndex = 0;
                });
            });
            
            SidePanelService.DrawMenu();
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageReinit('home', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageBeforeAnimation('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';

            ClearHistory();
            LoadCities(function (cities) {
                LoadCategories(function (result) {
                    InitializeTabsWidth();
                    $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
                    $('#linkHomeBannerCity .item-after').html('');
                    $scope.selectedItem = $rootScope.currentUserCity;
                    $scope.cites = myApp.fw7.Cities;
                    currentTabIndex = 0;
                    $scope.currentTabIndex = 0;
                });
            });

            SidePanelService.DrawMenu();
            
            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('home', function (page) {
            if ($rootScope.currentOpeningPage != 'home') return;
            $rootScope.currentOpeningPage = 'home';

            SidePanelService.DrawMenu();
            ClearHistory();
        });

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.ChangeTabPage = function (category, event) {
            var divTabs = document.getElementById('divTabs');
            var indexToTranslate = (window.innerWidth > 0) ? window.innerWidth : screen.width;
            indexToTranslate = (indexToTranslate * parseInt(category.index));
            divTabs.style.transform = 'translate3d(' + indexToTranslate + 'px, 0px, 0px)';
            divTabs.style.webkitTransform = 'translate3d(' + indexToTranslate + 'px, 0px, 0px)';

            currentTabIndex = parseInt(category.index);
            $scope.currentTabIndex = parseInt(category.index);

            $('.tab-link').removeClass('active');
            $(event.target).addClass('active');
        };

        $scope.GoToAgents = function (subCategory) {
            helpers.GoToPage('agents', { subCategoryId: subCategory.id });
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        app.init();
    });

}]);

