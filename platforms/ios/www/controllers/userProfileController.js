﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userProfileController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var isOwner = false;

    function LoadUserData(user) {
        $scope.username = typeof user != 'undefined' && user != null && typeof user.username != 'undefined' && user.username != null ? user.username : '';
        $scope.name = typeof user != 'undefined' && user != null && typeof user.name != 'undefined' && user.name != null ? user.name : '';
        $scope.mobile = typeof user != 'undefined' && user != null && typeof user.phone != 'undefined' && user.phone != null ? user.phone : '';
        $scope.cityName = typeof user != 'undefined' && user != null && user.city !=null && typeof user.city.name != 'undefined' && user.city.name != null ? user.city.name : '';

        $scope.IsUserNameExists = typeof user != 'undefined' && user.username != null && user.username != '' && user.username != ' ' ? true : false;
        $scope.IsNameExists = typeof user != 'undefined' && user.name != null && user.name != '' && user.name != ' ' ? true : false;
        $scope.IsMobileExists = typeof user != 'undefined' && user.phone != null && user.phone != '' && user.phone != ' ' ? true : false;
        $scope.IsCityExists = typeof user != 'undefined' && user.city != null && user.city.name != '' && user.city.name != ' ' ? true : false;

        if (typeof user.image != 'undefined' && user.image != null && user.image != '' && user.image != ' ') {
            $scope.photoUrl = user.image;
        }
        else {
            $scope.photoUrl = 'http://placehold.it/100?text=image';
        }
    }

    $document.ready(function () {
        app.onPageInit('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';

        });

        app.onPageBeforeAnimation('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';

            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
            appServices.CallService('userProfile', 'POST', "api/v1/profile", '', function (userData) {
                SpinnerPlugin.activityStop();
                if (userData) {
                    CookieService.setCookie('userLoggedIn', JSON.stringify(userData));
                    LoadUserData(userData);
                }

                setTimeout(function () {
                    $scope.$apply();
                }, fw7.DelayBeforeScopeApply);
            });

            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('userProfile', function (page) {
            if ($rootScope.currentOpeningPage != 'userProfile') return;
            $rootScope.currentOpeningPage = 'userProfile';           
        
        });

        app.onPageReinit('userProfile', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        $scope.GoToPreviousPage = function () {
            helpers.GoBack();
        };

        $scope.openEditProfile = function (userType) {
            helpers.GoToPage('editProfile', { userType: userType });
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        $scope.EditMobile = function () {
            app.prompt('من فضلك أدخل رقم الجوال الجديد', 'رقم الجوال الجديد', function (value) {

                if (helpers.isNumber(value) && value.match(/^(009665|9665|\+9665|05|5)(5|0|3|6|4|9|1|8|7)([0-9]{7})$/)) {
                    SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

                    var params = {
                        'phone': value
                    };

                    appServices.CallService('userProfile', 'POST', "api/v1/phone/edit", params, function (settings) {
                        SpinnerPlugin.activityStop();
                        if (settings != null) {
                            CookieService.setCookie('NewMobileToEdit', value);
                            helpers.GoToPage('activation', null);
                        }
                    });
                }
                else {
                    language.openFrameworkModal('تنبيه', 'من فضلك أدخل رقم جوال صحيح', 'alert', function () { });
                }
            });
        };

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        app.init();
    });

}]);

