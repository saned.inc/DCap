﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('landingController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'helpers', '$interval', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, helpers, $interval) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
        console.log('ready');
    }
    
    $document.ready(function () {

        app.onPageInit('landing', function (page) {
            if ($rootScope.currentOpeningPage != 'landing') return;
        });

        app.onPageAfterAnimation('landing', function (page) {
            if ($rootScope.currentOpeningPage != 'landing') return;
        });

        app.init();
    });

}]);