﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('searchController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    var allSubCategories = [];


    function LoadCities(callBack) {
        SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        var cities = [];

        appServices.CallService('search', "GET", "api/v1/cities", '', function (res) {
            if (res != null && res.length > 0) {
                $scope.cities = res;
                $rootScope.currentUserCity = $scope.cities[0];
                $scope.CurrentUserCityName = $rootScope.currentUserCity.name;
                myApp.fw7.Cities = res;
            }

            callBack(cities);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });


    }

    function LoadCategories(callBack) {
        var categories = [];
        allSubCategories = [];

        appServices.CallService('search', "GET", "api/v1/categories", '', function (res) {
            SpinnerPlugin.activityStop();
            if (res != null && res.length > 0) {
                angular.forEach(res, function (category, index) {
                    angular.forEach(category.children, function (subCategory, index) {
                        allSubCategories.push(subCategory);
                    });
                });

                $scope.subCategories = allSubCategories;
                $scope.Categories = res;
            }

            callBack(categories);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $document.ready(function () {
        app.onPageInit('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';

        });

        app.onPageBeforeAnimation('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';

            $scope.resetForm();

            LoadCities(function (cities) {
                LoadCategories(function (result) {
                    $scope.cites = myApp.fw7.Cities;
                });
            });
        });

        app.onPageReinit('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';

            $scope.resetForm();
        });

        app.onPageAfterAnimation('search', function (page) {
            if ($rootScope.currentOpeningPage != 'search') return;
            $rootScope.currentOpeningPage = 'search';

            $scope.resetForm();
        });

        $scope.FillSubCategories = function (category) {
            var filteredSubCategories = [];
            filteredSubCategories = allSubCategories.filter(function (subCategory) {
                return subCategory.parent_id == category.id;
            });

            $scope.subCategories = filteredSubCategories;
        };

        $scope.resetForm = function () {
            $scope.searchForm.name = null;
            $scope.selectedItem = null;
            $scope.selectedCategory = null;
            $scope.selectedSubCategory = null;

            //$('#linkSearchCity .item-inner').html('اختر المدينة');
            $('#linkSearchCity .item-after').html('اختر المدينة');

            //$('#linkSearchCategory .item-inner').html('التصنيفات الرئيسية');
            $('#linkSearchCategory .item-after').html('التصنيفات الرئيسية');

            //$('#linkSearchSubCategory .item-inner').html('التصنيفات الفرعية');
            $('#linkSearchSubCategory .item-after').html('التصنيفات الفرعية');

            if (typeof $scope.SearchForm != 'undefined' && $scope.SearchForm != null) {
                $scope.SearchForm.$setPristine(true);
                $scope.SearchForm.$setUntouched();
            }

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        }



        $scope.submitForm = function (isValid) {
            var city = '';
            var subCategory = '';
            var agentName = $scope.searchForm.name;

            agentName = typeof $scope.searchForm.name != 'undefined' && $scope.searchForm.name != null ? $scope.searchForm.name : '';
            city = typeof $scope.selectedItem != 'undefined' && $scope.selectedItem != null ? $scope.selectedItem.id : '';
            subCategory = typeof $scope.selectedSubCategory != 'undefined' && $scope.selectedSubCategory != null ? $scope.selectedSubCategory.id : '';

            helpers.GoToPage('agents', { name: agentName, city: city, subCategory: subCategory });
        };

        $scope.form = {};
        $scope.searchForm = {};

        app.init();
    });

}]);

