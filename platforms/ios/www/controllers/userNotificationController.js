﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userNotificationController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;
    var isNotificationsPull = true;
    var loadingNotifications = false;
    var allUserNotifications = [];

    function LoadNotifications(callBack) {
        if (!isNotificationsPull) {
            SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });
        }

        allUserNotifications = [];
        app.attachInfiniteScroll('#divInfiniteUserNotifications');

        var params = {
            'page_size': fw7.PageSize,
            'page': 0
        };

        appServices.CallService('userNotification', "POST", "api/v1/notifications", params, function (res) {
            if (!isNotificationsPull) {
                SpinnerPlugin.activityStop();
            }
            if (res.data != null && res.data.length > 0) {
                $$('.page[data-page="userNotification"]').removeClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'relative');

                angular.forEach(res.data, function (notification) {
                    notification.color = notification.data.color;
                    notification.icon = notification.data.icon;
                    notification.text = notification.data.text;

                    allUserNotifications.push(notification);
                });

                $scope.Notifications = allUserNotifications;
            }
            else {
                $$('.page[data-page="userNotification"]').addClass('noResult');
                $$('.pull-to-refresh-layer').css('position', 'absolute');
            }

            callBack(allUserNotifications);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });
    }

    $document.ready(function () {
        app.onPageInit('userNotification', function (page) {
            if ($rootScope.currentOpeningPage != 'userNotification') return;
            $rootScope.currentOpeningPage = 'userNotification';

            $$('#divInfiniteUserNotifications').on('ptr:refresh', function (e) {
                isNotificationsPull = true;
                CookieService.setCookie('userNotifications-page-number', 1);
                app.attachInfiniteScroll('#divInfiniteUserNotifications');
                LoadNotifications(function (result) {
                    app.pullToRefreshDone();
                });
            });

            $$('#divInfiniteUserNotifications').on('infinite', function () {
                if (loadingNotifications) return;
                loadingNotifications = true;

                SpinnerPlugin.activityStart("تحميل ...", { dimBackground: true });

                CookieService.setCookie('userNotifications-page-number', parseInt(CookieService.getCookie('userNotifications-page-number')) + 1);

                var notificationsParamter = {
                    'page_size': fw7.PageSize,
                    'page': parseInt(CookieService.getCookie('userNotifications-page-number'))
                };

                appServices.CallService('userNotification', 'POST', "api/v1/notifications", notificationsParamter, function (notifications) {
                    SpinnerPlugin.activityStop();
                    if (notifications.data && notifications.data.length > 0) {
                        loadingNotifications = false;

                        angular.forEach(notifications.data, function (notification) {
                            notification.color = notification.data.color;
                            notification.icon = notification.data.icon;
                            notification.text = notification.data.text;

                            allUserNotifications.push(notification);
                        });

                        if (notifications.data && notifications.data.length <= fw7.PageSize) {
                            app.detachInfiniteScroll('#divInfiniteUserNotifications');
                            return;
                        }

                        $scope.Notifications = allUserNotifications;
                    }
                    else {
                        app.detachInfiniteScroll('#divInfiniteUserNotifications');
                        loadingNotifications = false;
                    }

                    setTimeout(function () {
                        $scope.$apply();
                    }, fw7.DelayBeforeScopeApply);
                });
            });

        });

        app.onPageReinit('userNotification', function (page) {
            $scope.selectedItem = $rootScope.currentUserCity;
            CookieService.setCookie('userNotifications-page-number', 1);

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageBeforeAnimation('userNotification', function (page) {
            if ($rootScope.currentOpeningPage != 'userNotification') return;
            $rootScope.currentOpeningPage = 'userNotification';
            CookieService.setCookie('userNotifications-page-number', 1);

            isNotificationsPull = false;
            $scope.isPageLoaded = false;
            CookieService.setCookie('userNotifications-page-number', 1);

            LoadNotifications(function (result) {
                $scope.isPageLoaded = true;
            });


            $('#linkHomeBannerCity .item-inner').html($rootScope.currentUserCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $scope.selectedItem = $rootScope.currentUserCity;
            $scope.cites = myApp.fw7.Cities;

            setTimeout(function () {
                $scope.$apply();
            }, fw7.DelayBeforeScopeApply);
        });

        app.onPageAfterAnimation('userNotification', function (page) {
            if ($rootScope.currentOpeningPage != 'userNotification') return;
            $rootScope.currentOpeningPage = 'userNotification';
            
        });

        $scope.GoToSearch = function () {
            helpers.GoToPage('search', null);
        };

        $scope.StoreSelectedCity = function (selectedCity) {
            $('#linkHomeBannerCity .item-inner').html(selectedCity.name);
            $('#linkHomeBannerCity .item-after').html('');
            $rootScope.currentUserCity = selectedCity;
            $rootScope.cityId = selectedCity.id;
        };

        app.init();
    });

}]);

