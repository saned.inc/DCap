﻿/// <reference path="../js/angular.js" />

myApp.angular.controller('userTypeController', ['$document', '$scope', '$rootScope', '$http', 'InitService', '$log', 'appServices', 'CookieService', 'SidePanelService', 'helpers', function ($document, $scope, $rootScope, $http, InitService, $log, appServices, CookieService, SidePanelService, helpers) {
    'use strict';

    var fw7 = myApp.fw7;
    var app = myApp.fw7.app;

    function EnterApplication() {
        var isAndroid = Framework7.prototype.device.android === true;


        if (CookieService.getCookie('userLoggedIn')) {
            if (CookieService.getCookie('UserEntersCode') == "false") {
                helpers.GoToPage('activation', null);
            }
            else {
                if (isAndroid) {
                    helpers.GoToPage('homeIos', null);
                }
                else {
                    helpers.GoToPage('homeIos', null);
                }
            }
        }
        else {
            helpers.GoToPage('userType', null);
        }
    }

    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
        EnterApplication();
    }

    $(document).ready(function () {
        

        app.onPageInit('userType', function (page) {
            if ($rootScope.currentOpeningPage != 'userType') return;
            $rootScope.currentOpeningPage = 'userType';

        });

        app.onPageBeforeAnimation('userType', function (page) {
            if ($rootScope.currentOpeningPage != 'userType') return;
            $rootScope.currentOpeningPage = 'userType';

        });

        app.onPageReinit('userType', function (page) {
            if ($rootScope.currentOpeningPage != 'userType') return;
            $rootScope.currentOpeningPage = 'userType';

        });

        app.onPageAfterAnimation('userType', function (page) {
            if ($rootScope.currentOpeningPage != 'userType') return;
            $rootScope.currentOpeningPage = 'userType';

        });

        app.init();
    });

    $scope.GoToAgentSignup = function () {
        var isAndroid = Framework7.prototype.device.android === true;
        if (!CookieService.getCookie('userRole') || !CookieService.getCookie('userLoggedIn')) {
            CookieService.setCookie('userRole', 'agent');
        }

        if (CookieService.getCookie('userLoggedIn') && CookieService.getCookie('userRole') == 'agent') {
            CookieService.setCookie('userRole', 'agent');
            if (isAndroid) {
                helpers.GoToPage('homeIos', null);
            }
            else {
                helpers.GoToPage('homeIos', null);
            }
        }
        else {
            if (CookieService.getCookie('userLoggedIn') && CookieService.getCookie('userRole') != 'agent') CookieService.setCookie('userRole', 'agent');
            helpers.GoToPage('login', null);
        }
    };

    $scope.GoToUserSignup = function () {
        var isAndroid = Framework7.prototype.device.android === true;
        if (!CookieService.getCookie('userRole') || !CookieService.getCookie('userLoggedIn')) {
            CookieService.setCookie('userRole', 'seeker');
        }

        if (CookieService.getCookie('userLoggedIn') && CookieService.getCookie('userRole') == 'seeker') {
            CookieService.setCookie('userRole', 'seeker');
            if (isAndroid) {
                helpers.GoToPage('homeIos', null);
            }
            else {
                helpers.GoToPage('homeIos', null);
            }
        }
        else {
            if (CookieService.getCookie('userLoggedIn') && CookieService.getCookie('userRole') != 'seeker') CookieService.setCookie('userRole', 'seeker');
            helpers.GoToPage('login', null);
        }
    };

}]);

